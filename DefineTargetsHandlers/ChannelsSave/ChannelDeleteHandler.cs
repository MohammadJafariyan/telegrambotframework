﻿using System;
using System.Threading.Tasks;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBotsWebApplication.Models;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication.Handlers.ChannelsSave
{
    public class ChannelDeleteHandler : IMenuHandler
    {
        public string UniqueName
        {
            get { return "ChannelDeleteHandler"; }
        }
        private readonly ChannelService _channelService = new ChannelService();

        public async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs e)
        {
            
            var idstr = e.CallbackQuery.Data;
            int id;

            try
            {
                idstr=   MyGlobal.ExtractValueInlineQuery(idstr);
                id = int.Parse(idstr);
            }
            catch (Exception exception)
            {
                await Bot.Api.SendTextMessageAsync(
                    chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                    text:
                    exception.Message
                );
                return;
            }

            _channelService.DeleteById(id);

            var channels = _channelService.GetAll();

            var buttons = _channelService.ConvertToInlineKeybord(channels, "ChatId",this.UniqueName);


            var inlineKeyboard = new InlineKeyboardMarkup(buttons);
            await Bot.Api.SendTextMessageAsync(
                chatId: e.CallbackQuery.From.Id,
                text: "کانال مورد نظر حذف گردید ، لیست جدید بشرح زیر است",
                replyMarkup: inlineKeyboard
            );
        }

        public void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs e)
        {
        }

        public void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs e)
        {
        }

        public async void ShowInitialMenu(string msg = null)
        {
            /*if (msg==null)
            {
                await Bot.Api.SendTextMessageAsync(
                    chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                    text:
                    "لیست کانال ها به شرح زیر است"
                );
            }
            else
            {
                await Bot.Api.SendTextMessageAsync(
                    chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                    text:
                    msg
                );
            }*/
        }

        public async void BotOnMessageReceived(object sender, MessageEventArgs e)
        {
            var message = e.Message;
            //if (message == null || message.Type != MessageType.Text) return;

            //ShowInitialMenu();

          //  await Bot.Api.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);


            var channels = _channelService.GetAll();

            var buttons = _channelService.ConvertToInlineKeybord(channels, "ChatId",this.UniqueName);

            var inlineKeyboard = new InlineKeyboardMarkup(buttons);
            await Bot.Api.SendTextMessageAsync(
                chatId: message.Chat.Id,
                text: "برای حذف دکمه آن را لمس نمایید",
                replyMarkup: inlineKeyboard
            );


            /*
            switch (message.Text.Split(' ').First())
            {
                // send inline keyboard
                case "/inline":
                    await Bot.Api.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                    // simulate longer running task
                    await Task.Delay(500);

                    var inlineKeyboard = new InlineKeyboardMarkup(new[]
                    {
                        // first row
                        new []
                        {
                            InlineKeyboardButton.WithCallbackData("1.1", "11"),
                            InlineKeyboardButton.WithCallbackData("1.2", "12"),
                        },
                        // second row
                        new []
                        {
                            InlineKeyboardButton.WithCallbackData("2.1", "21"),
                            InlineKeyboardButton.WithCallbackData("2.2", "22"),
                        }
                    });
                    await Bot.Api.SendTextMessageAsync(
                        chatId: message.Chat.Id,
                        text: "Choose",
                        replyMarkup: inlineKeyboard
                    );
                    break;

                // send custom keyboard
                case "/keyboard":
                    ReplyKeyboardMarkup ReplyKeyboard = new[]
                    {
                        new[] { "1.1", "1.2" },
                        new[] { "2.1", "2.2" },
                    };
                    await Bot.Api.SendTextMessageAsync(
                        chatId: message.Chat.Id,
                        text: "Choose",
                        replyMarkup: ReplyKeyboard
                    );
                    break;

                // send a photo
                case "/photo":
                    await Bot.Api.SendChatActionAsync(message.Chat.Id, ChatAction.UploadPhoto);

                    const string file = @"Files/tux.png";
                    using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        var fileName = file.Split(Path.DirectorySeparatorChar).Last();
                        await Bot.Api.SendPhotoAsync(
                            chatId: message.Chat.Id,
                            photo: new InputOnlineFile(fileStream, fileName),
                            caption: "Nice Picture"
                        );
                    }
                    break;

                // request location or contact
                case "/request":
                    var RequestReplyKeyboard = new ReplyKeyboardMarkup(new[]
                    {
                        KeyboardButton.WithRequestLocation("Location"),
                        KeyboardButton.WithRequestContact("Contact"),
                    });
                    await Bot.Api.SendTextMessageAsync(
                        chatId: message.Chat.Id,
                        text: "Who or Where are you?",
                        replyMarkup: RequestReplyKeyboard
                    );
                    break;

                default:
                    const string usage = "Usage:\n" +
                        "/inline   - send inline keyboard\n" +
                        "/keyboard - send custom keyboard\n" +
                        "/photo    - send a photo\n" +
                        "/request  - request location or contact";
                    await Bot.Api.SendTextMessageAsync(
                        chatId: message.Chat.Id,
                        text: usage,
                        replyMarkup: new ReplyKeyboardRemove()
                    );
                    break;
            }
        */
        }

        private async Task<SocialChannel> GetChannelIfNotExistGoFirstStep(Message message)
        {
            var temporary = CurrentRequestHolderSingleton.CurrentRequest.GetTemporary();

            SocialChannel channel = null;
            try
            {
                channel = _channelService.FindById(temporary.ChannelId);
                return channel;
            }
            catch (RecordNotFoundException exception)
            {
                await Bot.Api.SendTextMessageAsync(
                    chatId: message.Chat.Id,
                    text: "کانال یافت نشد ، از نو وارد نمایید "
                );

                await Bot.Api.SendTextMessageAsync(
                    chatId: message.Chat.Id,
                    text:
                    "کد مخصوص کانال یا نام کاربری سوپر گروه مورد نظر یا نام کانال به فرمت @channelusername را وارد نمایید"
                );
                UserService.SetNextStepToCome(0);
                return null;
            }
        }

        public void BotOnReceiveError(object sender, ReceiveErrorEventArgs e)
        {
        }
    }
}