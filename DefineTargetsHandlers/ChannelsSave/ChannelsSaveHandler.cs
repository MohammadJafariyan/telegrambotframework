﻿using System;
using System.Threading.Tasks;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBotsWebApplication.Models;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication.Handlers.ChannelsSave
{
    /*
     * in exceptions must UserService.SetNextStepToCome(0);
     * in done of procedures (mean handlers) UserService.SetNextStepToCome(0);
     * فرم ثبت کانال
     */
    public abstract class ChannelsSaveHandler : IMenuHandler
    {

    
        public virtual string UniqueName
        {
            get { return "ChannelsSaveHandler"; }
        }

        private readonly ChannelService _channelService = new ChannelService();
        protected ChannelType ChannelType;
        protected SocialChannelType SocialChannelType;
        protected string InitialMessage;

        public void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs e)
        {
        }

        public void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs e)
        {
        }

        public void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs e)
        {
        }

        public async void ShowInitialMenu(string msg=null)
        {
            if (msg==null)
            {
                await Bot.Api.SendTextMessageAsync(
                    chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                    text:
                    "کد مخصوص کانال یا نام کاربری سوپر گروه مورد نظر یا نام کانال به فرمت @channelusername را وارد نمایید"
                );
            }
            else
            {
                await Bot.Api.SendTextMessageAsync(
                    chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                    text:
                    msg
                );
            }

            
        }
        public async void BotOnMessageReceived(object sender, MessageEventArgs e)
        {
            var message = e.Message;
            //if (message == null || message.Type != MessageType.Text) return;

            if (CurrentRequestHolderSingleton.CurrentRequest.User.NextStepToCome == 0)
            {
                ShowInitialMenu(InitialMessage ??  "کد مخصوص کانال یا نام کاربری سوپر گروه مورد نظر یا نام کانال به فرمت @channelusername را وارد نمایید");
                
                var channelId =  _channelService.Save(new SocialChannel
                {
                    ChannelType = ChannelType,
                    SocialChannelType = SocialChannelType,
                    TelegramUserId = CurrentRequestHolderSingleton.CurrentRequest.User.Id
                });

                UserService.SetNextStepToCome(1,
                    new TemporaryData
                    {
                        ChannelId = channelId
                    });
            }

            if (CurrentRequestHolderSingleton.CurrentRequest.User.NextStepToCome == 1)
            {
                
               //First 
               /// کاربر از مرحله قبل کد چت را وارد نمده است 
               SocialChannel channel =await GetChannelIfNotExistGoFirstStep(message);

             //  channel.ChatId = message.Text;
               _channelService.Update(channel);

             
               //Next
               
               ShowInitialMenu(
                   "آدرس کانال را وارد نمایید");
               
                UserService.SetNextStepToCome(3);
            }

            if (CurrentRequestHolderSingleton.CurrentRequest.User.NextStepToCome == 3)
            {
                //First 
               
                SocialChannel channel =await GetChannelIfNotExistGoFirstStep(message);
                
                channel.Address = message.Text;
                _channelService.Update(channel);
             
                
                //Next

                
                
                // این دو خط باید در آخر هر فرم باشد
                UserService.SetNextStepToCome(0);
                CurrentRequestHolderSingleton.SaveStateAndShowNextToComeMenu(BigMenuTreeHolderSingleton.MenuTree.Name);
              
            }

            /*
            switch (message.Text.Split(' ').First())
            {
                // send inline keyboard
                case "/inline":
                    await Bot.Api.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                    // simulate longer running task
                    await Task.Delay(500);

                    var inlineKeyboard = new InlineKeyboardMarkup(new[]
                    {
                        // first row
                        new []
                        {
                            InlineKeyboardButton.WithCallbackData("1.1", "11"),
                            InlineKeyboardButton.WithCallbackData("1.2", "12"),
                        },
                        // second row
                        new []
                        {
                            InlineKeyboardButton.WithCallbackData("2.1", "21"),
                            InlineKeyboardButton.WithCallbackData("2.2", "22"),
                        }
                    });
                    await Bot.Api.SendTextMessageAsync(
                        chatId: message.Chat.Id,
                        text: "Choose",
                        replyMarkup: inlineKeyboard
                    );
                    break;

                // send custom keyboard
                case "/keyboard":
                    ReplyKeyboardMarkup ReplyKeyboard = new[]
                    {
                        new[] { "1.1", "1.2" },
                        new[] { "2.1", "2.2" },
                    };
                    await Bot.Api.SendTextMessageAsync(
                        chatId: message.Chat.Id,
                        text: "Choose",
                        replyMarkup: ReplyKeyboard
                    );
                    break;

                // send a photo
                case "/photo":
                    await Bot.Api.SendChatActionAsync(message.Chat.Id, ChatAction.UploadPhoto);

                    const string file = @"Files/tux.png";
                    using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        var fileName = file.Split(Path.DirectorySeparatorChar).Last();
                        await Bot.Api.SendPhotoAsync(
                            chatId: message.Chat.Id,
                            photo: new InputOnlineFile(fileStream, fileName),
                            caption: "Nice Picture"
                        );
                    }
                    break;

                // request location or contact
                case "/request":
                    var RequestReplyKeyboard = new ReplyKeyboardMarkup(new[]
                    {
                        KeyboardButton.WithRequestLocation("Location"),
                        KeyboardButton.WithRequestContact("Contact"),
                    });
                    await Bot.Api.SendTextMessageAsync(
                        chatId: message.Chat.Id,
                        text: "Who or Where are you?",
                        replyMarkup: RequestReplyKeyboard
                    );
                    break;

                default:
                    const string usage = "Usage:\n" +
                        "/inline   - send inline keyboard\n" +
                        "/keyboard - send custom keyboard\n" +
                        "/photo    - send a photo\n" +
                        "/request  - request location or contact";
                    await Bot.Api.SendTextMessageAsync(
                        chatId: message.Chat.Id,
                        text: usage,
                        replyMarkup: new ReplyKeyboardRemove()
                    );
                    break;
            }
        */
        }

        private async Task<SocialChannel> GetChannelIfNotExistGoFirstStep(Message message)
        {
            var temporary = CurrentRequestHolderSingleton.CurrentRequest.GetTemporary();

            SocialChannel channel = null;
            try
            {
                channel =  _channelService.FindById(temporary.ChannelId);
                return channel;
            }
            catch (RecordNotFoundException exception)
            {
                await Bot.Api.SendTextMessageAsync(
                    chatId: message.Chat.Id,
                    text: "کانال یافت نشد ، از نو وارد نمایید "
                );
                  
                await Bot.Api.SendTextMessageAsync(
                    chatId: message.Chat.Id,
                    text:
                    "کد مخصوص کانال یا نام کاربری سوپر گروه مورد نظر یا نام کانال به فرمت @channelusername را وارد نمایید"
                );
                UserService.SetNextStepToCome(0);
                return null;

            }
        }

        public void BotOnReceiveError(object sender, ReceiveErrorEventArgs e)
        {
        }
    }
}