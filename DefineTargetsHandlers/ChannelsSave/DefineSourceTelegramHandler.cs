﻿using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Handlers.ChannelsSave
{
    public class DefineSourceTelegramHandler : ChannelsSaveHandler
    {
        public override string UniqueName
        {
            get { return "DefineSourceTelegramHandler"; }
        }
        public DefineSourceTelegramHandler()
        {
            ChannelType= ChannelType.Source;
            SocialChannelType= SocialChannelType.Telegram;
        }
    }
}