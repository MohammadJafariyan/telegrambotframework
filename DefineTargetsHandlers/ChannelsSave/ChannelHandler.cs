﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBotsWebApplication.Models;
using TelegramBotsWebApplication.Service;
using File = Telegram.Bot.Types.File;

namespace TelegramBotsWebApplication.Handlers.ChannelsSave
{
    public class ChannelHandler : IMenuHandler
    {
        
        public void ShowInitialMenu(string msg)
        {
            
        }

        public string UniqueName
        {
            get { return "ChannelListHandler"; }
        }
        private readonly ChannelService _channelService = new ChannelService();

        public void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs e)
        {
        }

        public void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs e)
        {
        }

        public void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs e)
        {
        }


        public async void BotOnMessageReceived(object sender, MessageEventArgs e)
        {
            List<Feed> feeds = _channelService.GetTelegramSourceIfExists(CurrentRequestHolderSingleton.CurrentRequest.User.Id,
                e.Message.Chat.Id);

            foreach (var feed in feeds)
            {
                
                switch (e.Message.Type)
                {
                    case Telegram.Bot.Types.Enums.MessageType.Text:

                        try
                        {
                            var isCorrect = CheckCorrectness(feed, e);
                            if (isCorrect==false)
                            {
                                          //      await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                                return;
                            }
                            
                            var txt=e.Message.Text;
                            if (txt != null)
                            {
                                await Bot.Api.SendTextMessageAsync(feed.TelegramChatId, txt);
                            }
                           

                        }
                        catch (Exception)
                        {

                            throw;
                        }

                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Photo:

                        var FileId = e.Message.Photo[0].FileId;
                        var fileInfo = await Bot.Api.GetFileAsync(FileId);

                        var filename = fileInfo.FileId + "." + fileInfo.FilePath.Split('.').Last();
                        var saveImageStream = System.IO.File.Open(filename, System.IO.FileMode.Create, FileAccess.ReadWrite);

                        await Bot.Api.DownloadFileAsync(fileInfo.FilePath, saveImageStream, CancellationToken.None);
                        saveImageStream.Dispose();
                        var saveImageStream2 = System.IO.File.Open(filename, FileMode.Open, FileAccess.ReadWrite);
                        try
                        {
                            var isCorrect = CheckCorrectness(feed, e);
                            if (isCorrect==false)
                            {
                                //      await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                                return;
                            }
                            var txt = e.Message.Caption;
                            if (txt != null)
                            {
                                await Bot.Api.SendPhotoAsync(feed.TelegramChatId, new Telegram.Bot.Types.InputFiles.InputOnlineFile(saveImageStream2), txt);
                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        System.IO.File.Delete(filename);




                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Audio:
                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Video:

                        FileId = e.Message.Video.FileId;
                        fileInfo = await  Bot.Api.GetFileAsync(FileId);

                        filename = fileInfo.FileId + "." + fileInfo.FilePath.Split('.').Last();
                        saveImageStream = System.IO.File.Open(filename, System.IO.FileMode.Create, FileAccess.ReadWrite);

                        await Bot.Api.DownloadFileAsync(fileInfo.FilePath, saveImageStream, CancellationToken.None);
                        saveImageStream.Dispose();
                        saveImageStream2 = System.IO.File.Open(filename, FileMode.Open, FileAccess.ReadWrite);
                        try
                        {
                            var isCorrect = CheckCorrectness(feed, e);
                            if (isCorrect==false)
                            {
                                //      await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                                return;
                            }
                            var txt = e.Message.Caption;
                            if (txt != null)
                            {
                                await Bot.Api.SendVideoAsync(feed.TelegramChatId, new Telegram.Bot.Types.InputFiles.InputOnlineFile(saveImageStream2), 0, 0, 0, txt);
                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        System.IO.File.Delete(filename);



                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Voice:
                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Document:

                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Sticker:
                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Location:
                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Contact:
                        break;

                    case Telegram.Bot.Types.Enums.MessageType.ChatPhotoDeleted:
                        break;

                    default:

                        break;
                }
            }


            /*var LstSourceChanel = repositorySour.GetAll().Where(x=>x.ChannelId==e.Message.Chat.Id.ToString()).ToList();
            

            foreach (var item in LstSourceChanel)
            {
                
                switch (e.Message.Type)
                {
                    case Telegram.Bot.Types.Enums.MessageType.Text:

                        try
                        {
                            
                            var txt = FilteredTxtMessage(item.Target.Id, e.Message.Text);
                            if (txt != null)
                            {
                                await TelBotclient.SendTextMessageAsync(item.Target.ChannelId, txt);

                            }

                        }
                        catch (Exception)
                        {

                            throw;
                        }

                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Photo:

                        var FileId = e.Message.Photo[0].FileId;
                        var fileInfo = await TelBotclient.GetFileAsync(FileId);

                        var filename = fileInfo.FileId + "." + fileInfo.FilePath.Split('.').Last();
                        var saveImageStream = System.IO.File.Open(filename, System.IO.FileMode.Create, FileAccess.ReadWrite);

                        await TelBotclient.DownloadFileAsync(fileInfo.FilePath, saveImageStream, CancellationToken.None);
                        saveImageStream.Dispose();
                        var saveImageStream2 = System.IO.File.Open(filename, FileMode.Open, FileAccess.ReadWrite);
                        try
                        {
                            var txt = FilteredTxtMessage(item.Target.Id, e.Message.Caption);
                            if (txt != null)
                            {
                                await TelBotclient.SendPhotoAsync(item.Target.ChannelId, new Telegram.Bot.Types.InputFiles.InputOnlineFile(saveImageStream2), txt);
                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        File.Delete(filename);




                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Audio:
                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Video:

                        FileId = e.Message.Video.FileId;
                        fileInfo = await TelBotclient.GetFileAsync(FileId);

                        filename = fileInfo.FileId + "." + fileInfo.FilePath.Split('.').Last();
                        saveImageStream = System.IO.File.Open(filename, System.IO.FileMode.Create, FileAccess.ReadWrite);

                        await TelBotclient.DownloadFileAsync(fileInfo.FilePath, saveImageStream, CancellationToken.None);
                        saveImageStream.Dispose();
                        saveImageStream2 = System.IO.File.Open(filename, FileMode.Open, FileAccess.ReadWrite);
                        try
                        {
                            var txt = FilteredTxtMessage(item.Target.Id, e.Message.Caption);
                            if (txt != null)
                            {
                                await TelBotclient.SendVideoAsync(item.Target.ChannelId, new Telegram.Bot.Types.InputFiles.InputOnlineFile(saveImageStream2), 0, 0, 0, txt);
                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        File.Delete(filename);



                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Voice:
                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Document:

                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Sticker:
                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Location:
                        break;
                    case Telegram.Bot.Types.Enums.MessageType.Contact:
                        break;

                    case Telegram.Bot.Types.Enums.MessageType.ChatPhotoDeleted:
                        break;

                    default:

                        break;
                }
            }*/
        }

        private bool CheckCorrectness(Feed feed, MessageEventArgs e)
        {
            if (feed.IsForwaredPostNotSendToChannel && e.Message.IsForwarded)
            {
                return false;
            }

              
            if (feed.IsHasJoinUrlNotSendToChannel)
            {
                if (e.Message.Text.Contains(e.Message.Chat.InviteLink) )
                {
                    return false;
                }
                
                if (e.Message.Text.Contains(e.Message.Chat.Username) )
                {
                    return false;
                }
            }
            
            if (feed.IsHasUrlNotSendToChannel)
            {
                if (e.Message.Text.Contains(e.Message.Chat.Username) )
                {
                    return false;
                }
                if (e.Message.Text.Contains("http") || e.Message.Text.Contains("www")  
                                                            || e.Message.Text.Contains(".ir") || e.Message.Text.Contains(".com"))
                {
                    return false;
                }
            }

            return true;

        }


        public void BotOnReceiveError(object sender, ReceiveErrorEventArgs e)
        {
        }
    }
}