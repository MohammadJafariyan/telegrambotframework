﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramBotsWebApplication.Handlers;
using TelegramBotsWebApplication.Handlers.ChannelsSave;
using TelegramBotsWebApplication.Models;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication
{
    
    /// <summary>
    ///  اولین کلاسی که با پیغام های تلگرام درگیر می شود
    /// بعد بر اساس نوع پیغام آن را به کلاس های دیگر می دهد
    /// تمامی کلاس های درگیر شونده با پیغام های تلگرام
    /// handler
    /// هستند
    /// </summary>
    public class MessageDispatcher : IMenuHandler
    {
        private IMenuHandler _handler;
        private readonly UserService _userService = new UserService();

        public async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs e)
        {
            try
            {

                EveryRequestInitialize(e.CallbackQuery.From.Id,
                    e.CallbackQuery.Message,
                    e.CallbackQuery.Message.Text,
                    e.CallbackQuery.Message.From.Username,
                    null,
                    e.CallbackQuery.Message.From.FirstName,
                    e.CallbackQuery.Message.From.LastName);

                _handler.BotOnCallbackQueryReceived(sender, e);
                /*SelectMenuByCallbackQuery(e.CallbackQuery.From.Id,
                    e.CallbackQuery.Message,
                    e.CallbackQuery.Data,
                    e.CallbackQuery.From.Username,
                    null,
                    e.CallbackQuery.From.FirstName,
                    e.CallbackQuery.From.LastName);

                _handler.BotOnCallbackQueryReceived(sender, e);*/
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                await Bot.Api.SendTextMessageAsync(
                    chatId: e.CallbackQuery.Message.From.Id,
                    text:
                    MyGlobal.RecursiveExecptionMsg(exception)
                );
            }
            
        }


        private void SelectMenuByCallbackQuery(int fromId, Message callbackQueryMessage, 
            string callbackQueryData, string fromUsername
            , object o, string fromFirstName, string fromLastName)
        {
            CurrentRequestHolderSingleton.CurrentRequest.InitializeForInlineQuery
                (callbackQueryData);
            _handler = CurrentRequestHolderSingleton.CurrentRequest.CurrentMenuTree.Handler;
        }

        public void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs e)
        {
            _handler.BotOnChosenInlineResultReceived(sender, e);
        }

        public void ShowInitialMenu(string msg)
        {
        }

        public string UniqueName
        {
            get { return "MessageDispatcher"; }
        }


        public void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs e)
        {
            _handler.BotOnInlineQueryReceived(sender, e);
        }

        public async void BotOnMessageReceived(object sender, MessageEventArgs e)
        {
            /*await Bot.Api.SendTextMessageAsync(
                chatId: e.Message.From.Id,
                text:
                "سلام خوش آمدید"
            );*/
            try
            {
                EveryRequestInitialize(e.Message.From.Id,
                    e.Message,
                    e.Message.Text,
                    e.Message.From.Username,
                    null,
                    e.Message.From.FirstName,
                    e.Message.From.LastName);

                _handler.BotOnMessageReceived(sender, e);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                await Bot.Api.SendTextMessageAsync(
                    chatId: e.Message.From.Id,
                    text:
                    MyGlobal.RecursiveExecptionMsg(exception)
                );                                                                                             
            }
        }

        private void EveryRequestInitialize(int telegramUserId
            , Message eMessage,
            string text,
            string username = null, string mobile = null,
            string firstName = null, string lastName = null)
        {
            dynamic tuple = _userService.CreateIfNotExist(telegramUserId,
                text,
                username,
                mobile,
                firstName,
                lastName);

            CurrentRequestHolderSingleton.CurrentRequest.Initialize(tuple, text, eMessage);

            if (eMessage.Chat.Type==ChatType.Private)
            {

                // اگر handler نال نباشد 
                // یعنی در پیغام های قبلی (کنتاکت های قبلی) کاربر به handler ای 
                // پاس داده شده است
                if (!string.IsNullOrWhiteSpace(CurrentRequestHolderSingleton.CurrentRequest.User.Handler))
                {
                    string handler= CurrentRequestHolderSingleton.CurrentRequest.User.Handler;
                    _handler = BigMenuTreeHolderSingleton.GetApproprateHandlerByName(handler);

                    // یعنی یافت نشد
                    if (_handler == null)
                    {
                        _handler = new HelloHandler();

                    }
                    return;
                }

               

                _handler =new HelloHandler();
            }
            else
            {
                _handler = new ChannelHandler();
            }

            /*
            _handler = CurrentRequestHolderSingleton.CurrentRequest.CurrentMenuTree.Handler;#1#
        */
        }
        
        /// <summary>
        /// روش قبلی با ین متد پیاده شده بود ، برای بازگشت به روش قبلی کافیست نوشته بکاپ را حذف نایید
        /// </summary>
        /// <param name="telegramUserId"></param>
        /// <param name="eMessage"></param>
        /// <param name="text"></param>
        /// <param name="username"></param>
        /// <param name="mobile"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        private void EveryRequestInitializeBackup(int telegramUserId
            , Message eMessage,
            string text,
            string username = null, string mobile = null,
            string firstName = null, string lastName = null)
        {
            dynamic tuple = _userService.CreateIfNotExist(telegramUserId,
                text,
                username,
                mobile,
                firstName,
                lastName);


            CurrentRequestHolderSingleton.CurrentRequest.Initialize(tuple, text, eMessage);
            _handler = CurrentRequestHolderSingleton.CurrentRequest.CurrentMenuTree.Handler;
        }

        public void BotOnReceiveError(object sender, ReceiveErrorEventArgs e)
        {
            
            //_handler.BotOnReceiveError(sender, e);
        }


        /*public void Dispatch(object sender, MessageEventArgs e)
        {

           
            Bot.Api.OnMessage += handler.BotOnMessageReceived;
            Bot.Api.OnMessageEdited += handler.BotOnMessageReceived;
            Bot.Api.OnCallbackQuery += handler.BotOnCallbackQueryReceived;
            Bot.Api.OnInlineQuery += handler.BotOnInlineQueryReceived;
            Bot.Api.OnInlineResultChosen += handler.BotOnChosenInlineResultReceived;
            Bot.Api.OnReceiveError += handler.BotOnReceiveError;

        }*/

        private MenuTree FindApproprateHandler(string msg)
        {
            return FindApproprateHandlerRecursive(msg, BigMenuTreeHolderSingleton.MenuTree);
        }

        public static MenuTree FindApproprateHandlerRecursive(string msg, MenuTree menuTree)
        {
            if (msg == menuTree.Name)
            {
                return menuTree;
            }

            foreach (var leaf in menuTree.Leafs)
            {
                var find=   FindApproprateHandlerRecursive(msg, leaf);
                if (find!=BigMenuTreeHolderSingleton.MenuTree)
                {
                    return find;
                }
            }

            // اگر پیدا نکرد به منو اصلی بر میگردد
            return BigMenuTreeHolderSingleton.MenuTree;
        }
        
        public static MenuTree FindApproprateHandlerRecursiveByInlineQuery(string msg, MenuTree menuTree)
        {
            if (msg == menuTree.Handler.UniqueName)
            {
                return menuTree;
            }

            foreach (var leaf in menuTree.Leafs)
            {
                var find=   FindApproprateHandlerRecursiveByInlineQuery(msg, leaf);
                if (find!=BigMenuTreeHolderSingleton.MenuTree && !(find.Handler is HelloHandler))
                {
                    return find;
                }
            }

            // اگر پیدا نکرد به منو اصلی بر میگردد
            return BigMenuTreeHolderSingleton.MenuTree;
        }
    }
}