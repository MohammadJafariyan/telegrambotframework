﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TelegramBotsWebApplication.Areas.Admin.Service;

namespace TelegramBotsWebApplication.Models
{
    public class Feed:IEntity
    {


        [NotMapped]
        public List<ExchaneArrayForFeed> ExchaneArrayForFeed { get; set; }

        public List<TelegramPost> TelegramPosts { get; set; }

        
        /// <summary>
        /// split by ,
        /// </summary>
        public string ExchangeWordsFrom { get; set; }
        
              
        /// <summary>
        /// split by ,
        /// </summary>
        public string ExchangeWordsTo { get; set; }
        public Feed()
        {
            TimeFrom=new DateTime(2020,1,1,0,0,0);
            TimeTo=new DateTime(2020,1,1,23,59,59);
        }
        [Key] public int Id { get; set; }

        public FeedType Type { get; set; }
        public string Address { get; set; }

        public SocialChannel SocialChannel { get; set; }
        public int? SocialChannelId { get; set; }

        public TelegramUser TelegramUser { get; set; }
        public int? TelegramUserId { get; set; }

        /// <summary>
        /// ارسال «ساکت» مطالب این فید (notification disabled)
        /// </summary>
        public bool NotificationDisabled { get; set; }
        
        /// <summary>
        /// حاوی لینک مطلب
        /// </summary>
        public bool IncludeArticleLink { get; set; }
        
        /// <summary>
        /// سعی کن عکس بالا باشد (متن برش می‌خورد تا مقدار زیرنویس به هزار کاراکتر برسد) 
        /// </summary>
        public bool TryPhotoUponArticle { get; set; }
        
        /// <summary>
        /// گر عکس نتوانست بالا باشد و ارسال متنی بود، فقط عکس به عنوان preview زیر مطلب درج شود
        /// </summary>
        public bool TryPhotoUponArticleAsPreviewIfCannot { get; set; }

        public int CheckTimesInADay { get; set; }
        
        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm}")]
        public DateTime TimeFrom { get; set; }
        
        
        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm}")]
        public DateTime TimeTo { get; set; }

        
        /// <summary>
        /// حذف هشتگ‌های انتهای متن
        /// </summary>
        public bool DeleteHashtagFromEnd { get; set; }

        /// <summary>
        /// حذف علامت هشتگ‌های از کلمات بین متن و تبدیل آن به کلمه (مثلا تبدیل #فصل_بهار به: فصل بهار)
        /// </summary>
        public bool DeleteHashtagSigns { get; set; }

        /// <summary>
        /// قبل از ارسال یک مطلب به کانال شما، چک می‌شود که مطلب دارای حداقل یکی از عبارات زیر هست یا خیر. اگر آن مطلب دارای حداقل یکی از عبارات زیر بود، آنگاه آن مطلب به کانال ارسال می‌شود و اگر دارای هیچ‌یک از کلمات زیر نباشد، به کانال ارسال نمی‌شود:
        /// </summary>
        public string WordsMustBeInside { get; set; }


        /// <summary>
        /// قبل از ارسال یک مطلب به کانال شما، چک می‌شود که مطلب دارای حداقل یکی از عبارات زیر هست یا خیر. اگر آن مطلب فاقد همه عبارات زیر بود، آنگاه آن مطلب به کانال ارسال می‌شود و اگر دارای حداقل یکی از کلمات زیر باشد، به کانال ارسال نمی‌شود: 

        /// </summary>
        public string WordsNotMustBeInside { get; set; }

        /// <summary>
        ///                     بجز تیتر و آدرس مطلب، چند کاراکتر از متن موجود در این فید در کانال درج شود؟

        /// </summary>
        public int? WordCountLimit { get; set; }

        /// <summary>
        /// که مثلا می‌تواند حاوی لینک عمومی یا لینک join کانال شما باشد همراه با اموجی (شکلک) و ... 
        /// </summary>
        public string SignEndOfPost { get; set; }

        /// <summary>
        /// اگر مطلب فورواردی بود، به کانالم نفرست
        /// </summary>
        public bool IsForwaredPostNotSendToChannel { get; set; }

        
        /// <summary>
        ///  اگر متن حاوی هر نوع لینک/URL ی بود، به کانالم نفرست
        /// </summary>
        public bool IsHasUrlNotSendToChannel { get; set; }

        /// <summary>
        /// اگر مطلب حاوی لینک جوین بود، به کانالم نفرست
        /// </summary>
        public bool IsHasJoinUrlNotSendToChannel { get; set; }

        public string LastPost { get; set; }
        public long TelegramChatId { get; set; }
    }
}