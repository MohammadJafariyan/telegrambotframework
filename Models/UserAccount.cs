﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TelegramBotsWebApplication.Areas.Admin.Service;

namespace TelegramBotsWebApplication.Models
{
    public class UserAccount:IEntity
    {

        [Key]
        public int Id { get; set; }
        public bool IsActive { get; set; }
        
        public DateTime ExpireDate { get; set; }

        public int MaxCheckInDayCount { get; set; }

        public int MaxFeedCount { get; set; }

        public UserAccountType UserAccountType { get; set; }

        public List<TelegramUser> TelegramUsers { get; set; }
    }

   
}