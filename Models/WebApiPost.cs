﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TelegramBotsWebApplication.Models
{


    /// <summary>
    /// آبجکت پست های وب سایت های شخص ثالث
    /// وب سایت های شخص ثالث می توانند با ارسال پست هایی از طریق وب سرویس
    /// به کانال های موجود در دیتابیس ما مطلب بفرستند
    /// </summary>
    public class WebApiPost
    {
        public WebApiPost()
        {
        }

        /// <summary>
        /// متن
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// اگر عکس باشد url عکس
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// عکس بصورت بایت ارایه
        /// </summary>
        public byte[] ImageContent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ImageExtention { get; set; }

        /// <summary>
        /// تاریخ ایجاد موقع دریافت از طریق وب سرویس زده می شود
        /// </summary>
        public DateTime? CreationDateTime { get; set; }

        /// <summary>
        /// تاریخ پست کردن که در توسط سایت شخص ثالث زده میشود
        /// </summary>
        public DateTime? PostDateTime { get; set; }

        /// <summary>
        /// تاریخ انقضا که اگر رد شود دیگر به کانال نخواهد فرستاد
        /// </summary>
        public DateTime? ExpireDateTime { get; set; }

        /// <summary>
        /// تعداد مشاهده که فعلا صفر خواهد بود و باید موقع لزوم به تلگرام پیام داده و مقدار آن پرسیده شود
        /// </summary>
        public int ViewCount { get; set; }

        /// <summary>
        /// اتصال پست با کانال تلگرام
        /// foreign key
        /// </summary>
        public int SocialChannelId { get; set; }

        /// <summary>
        /// کانال تلگرام مخصوص این پست 
        /// </summary>
        public SocialChannel SocialChannel { get; set; }
        
        [Key]
        public int Id { get; set; }
      
        /// <summary>
        /// وضعیت
        /// </summary>
        public WebApiPostStatus Status { get; set; }

        /// <summary>
        /// کد پیغام مخصوص در تلگرام
        /// </summary>
        public int TelegramMessagePostId { get; set; }

        /// <summary>
        /// آیا به کانال ارسال شده است
        /// </summary>
        public bool IsSentToChannel { get; set; }

        /// <summary>
        /// آیا حذف شده است
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}