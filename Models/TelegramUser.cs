﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TelegramBotsWebApplication.Areas.Admin.Service;

namespace TelegramBotsWebApplication.Models
{

    /// <summary>
    /// کاربر تلگرامی
    /// هر کاربری که پیغام دهد ثبت می گردد
    /// </summary>
    public class TelegramUser:IEntity
    {
        public TelegramUser()
        {
            TemporaryDatas = new List<TemporaryData>();
            TelegramPosts=new List<TelegramPost>();
        }

        [Key] public int Id { get; set; }


        /// <summary>
        /// نام کاربری
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// شماره تلفن
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// آخرین نوشته ارسالی به بات
        /// </summary>
        public string LastTypedText { get; set; }

        /// <summary>
        /// کد کاربر در تلگرام
        /// </summary>
        [Index(IsUnique = true)] public int TelegramUserId { get; set; }

        /// <summary>
        /// نام
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// نام خانوادگی
        /// </summary>
        public string LastName { get; set; }


        /// <summary>
        /// flag هایی که
        /// به کاربر زده می شود تا بفهمیم در کدام مرحله قرار دارد
        /// این مورد استفاده قرار نمی گیرد
        /// زیرا فرم های موجود در تلگرام کنار گذاشته شد
        /// </summary>
        public int NextStepToCome { get; set; }

        /// <summary>
        /// مورد استفاده در کد های فرم های موجود در خود تلگرام که مورد اسفتاده نیست
        /// </summary>
        public List<TemporaryData> TemporaryDatas { get; set; }

        /// <summary>
        /// مورد استفاده در کد های فرم های موجود در خود تلگرام که مورد اسفتاده نیست
        /// </summary>
        public string NextMenuNameToCome { get; set; }


        /// <summary>
        /// کانال های تلگرامی مبدا و مقصد تعریف شده برای این کاربر
        /// </summary>
        public List<SocialChannel> TelegramChannels { get; set; }



        /// <summary>
        /// هر کاربر تلگرامی یک اکانت هم دارد که برای خرید و تمدید اکانت مورد اسفتاده خواهد گرفت
        /// foreign key
        /// </summary>
        public int? UserAccountId { get; set; }

        /// <summary>
        /// هر کاربر تلگرامی یک اکانت هم دارد که برای خرید و تمدید اکانت مورد اسفتاده خواهد گرفت
        /// </summary>
        public UserAccount UserAccount { get; set; }
      
        /// <summary>
        /// کاربر در مراجعات بعدی (یعنی پیغام های بعدی) به handler ای که
        /// در این متد داده می شود پاس داده خواهد شد
        /// </summary>
        public string Handler { get; set; }

        public List<TelegramPost> TelegramPosts { get; set; }
        public List<Feed> Feeds { get; set; }

        /*
        public EntityList<InstagramChannel> InstagramChannels { get; set; }
    */
    }
}