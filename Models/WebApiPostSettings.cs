﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TelegramBotsWebApplication.Models
{
    /// <summary>
    /// تنظیمات کلی وب سریس ها
    /// </summary>
    public class WebApiPostSettings
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        ///  از چه ساعتی مجاز است پست های شخص ثالث را بفرستد
        /// </summary>
        public DateTime From { get; set; }

        /// <summary>
        ///  تا چه ساعتی مجاز نیست پست های شخص ثالث را بفرستد
        /// </summary>
        public DateTime To { get; set; }
    }
}