﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramBotsWebApplication.Areas.Admin.Service;

namespace TelegramBotsWebApplication.Models
{
    public class TelegramPost : IEntity
    {
        public TelegramPost()
        {
            Delivered = false;
            CreationDate = DateTime.Now;
        }

        [Key] public int Id { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string RssLink { get; set; }

        /// <summary>
        /// rss id
        /// </summary>
        public string UniqId { get; set; }

        public Feed Feed { get; set; }
        public int? FeedId { get; set; }
        public long Imagelength { get; set; }
        public string MediaType { get; set; }
        public string TelegramFileId { get; set; }
        public MessageType TelegramMessageType { get; set; }
        public bool Delivered { get; set; }
        public string Text { get; set; }
        public string Caption { get; set; }

        public int? TelegramUserId { get; set; }
        public TelegramUser TelegramUser { get; set; }

        public static string GetFileId(Message message)
        {
            if (string.IsNullOrEmpty(message.Audio?.FileId) == false)
            {
                return message.Audio?.FileId;
            }

            if (string.IsNullOrEmpty(message.Animation?.FileId) == false)
            {
                return message.Animation?.FileId;
            }

            if (string.IsNullOrEmpty(message.Document?.FileId) == false)
            {
                return message.Document?.FileId;
            }

            if (string.IsNullOrEmpty(message.Video?.FileId) == false)
            {
                return message.Video?.FileId;
            }

            if (string.IsNullOrEmpty(message.Voice?.FileId) == false)
            {
                return message.Voice?.FileId;
            }

            if (string.IsNullOrEmpty(message.Sticker?.FileId) == false)
            {
                return message.Sticker?.FileId;
            }

            if (message.Photo?.Length > 0)
            {
                return string.Join(",", message.Photo.Select(p => p.FileId).ToArray());
            }

            if (string.IsNullOrEmpty(message.VideoNote?.FileId) == false)
            {
                return message.VideoNote?.FileId;
            }

            return null;
        }

        public static TelegramPost GetPostFromMessage(Message message, int currUserId)
        {
            return new TelegramPost
            {
                TelegramMessageType = message.Type,
                TelegramMessagePostIds = new List<TelegramMessagePostViewModel>
                {
                    new TelegramMessagePostViewModel
                    {
                        ChannelId = message.Chat.Id,
                        MessageId = message.MessageId
                    }
                },
                Caption = message.Caption,
                TelegramFileId = TelegramPost.GetFileId(message),
                Text = message.Text,
                DeliveredChannelIds = null,
                CreationDate = DateTime.Now,
                TelegramUserId = currUserId
            };
        }

        public DateTime? CreationDate { get; set; }
        public DateTime? SendTime { get; set; }

        public bool? IsSendNow { get; set; }

        public string DeliveredChannelIds { get; set; }
        public string _TelegramMessagePostIds { get; set; }

        [NotMapped]
        public List<TelegramMessagePostViewModel> TelegramMessagePostIds
        {
            get
            {
                var vms = JsonConvert.DeserializeObject<List<TelegramMessagePostViewModel>>(_TelegramMessagePostIds);
                if (vms == null)
                {
                    vms = new List<TelegramMessagePostViewModel>();
                }

                return vms;
            }
            set { _TelegramMessagePostIds = JsonConvert.SerializeObject(value); }
        }

        public void PushToTelegramMessagePostIds(TelegramMessagePostViewModel vm)
        {
            //if exits throws
            var exist=TelegramMessagePostIds.Any(t => t.ChannelId == vm.ChannelId &&
                                                  t.MessageId == vm.MessageId);
            if (exist)
            {
                return;
            }
            var tmp = TelegramMessagePostIds;
            tmp.Add(vm);
            _TelegramMessagePostIds = JsonConvert.SerializeObject(tmp);
        }

        public void ReplaceInMessageIds(TelegramMessagePostViewModel telegramMessagePostViewModel,
            TelegramMessagePostViewModel update        )
        {
            var i = TelegramMessagePostIds.FindIndex(t => t.ChannelId == telegramMessagePostViewModel.ChannelId &&
                                                          t.MessageId == telegramMessagePostViewModel.MessageId);
            var tmp = TelegramMessagePostIds;
            tmp[i] = update;
            _TelegramMessagePostIds = JsonConvert.SerializeObject(tmp);
        }
    }

    public class TelegramMessagePostViewModel
    {
        public long ChannelId { get; set; }
        public int MessageId { get; set; }

        public TelegramMessagePostViewModel Clone()
        {
            var json=JsonConvert.SerializeObject(this);
            return JsonConvert.DeserializeObject<TelegramMessagePostViewModel>(json);
        }
    }
}