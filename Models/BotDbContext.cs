﻿using System.Data.Entity;
using TelegramBotsWebApplication.Migrations;

namespace TelegramBotsWebApplication.Models
{
    public class BotDbContext : DbContext
    {
        public BotDbContext()
            : base(
                
                //System.Diagnostics.Debugger.IsAttached ? "ashpazerooz.ir_development" :
                "ashpazerooz.ir")
        { 
              Database.SetInitializer(new MigrateDatabaseToLatestVersion<BotDbContext, Configuration>());
             // Database.SetInitializer(new DropCreateDatabaseAlways<BotDbContext>());

        }
        
        
         

        public virtual DbSet<TelegramUser> TelegramUsers { get; set; }
        public virtual DbSet<UserAccount> UserAccounts { get; set; }
        public virtual DbSet<TemporaryData> TemporaryDatas { get; set; }
        public virtual DbSet<SocialChannel> SocialChannels { get; set; }
        public virtual DbSet<Feed> Feeds { get; set; }
        public virtual DbSet<TelegramPost> TelegramPosts { get; set; }
        
        public virtual DbSet<WebApiPost> WebApiPosts { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<WebApiPostSettings> WebApiPostSettings { get; set; }
        
        /*
        public virtual DbSet<InstagramChannel> InstagramChannels { get; set; }
        */

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<TelegramUser>().HasMany(t => t.TemporaryDatas)
                .WithRequired(t => t.TelegramUser)
                .HasForeignKey(t => t.TelegramUserId).WillCascadeOnDelete(false);


            modelBuilder.Entity<TelegramUser>().HasMany(t => t.TelegramChannels)
                .WithRequired(t => t.TelegramUser)
                .HasForeignKey(t => t.TelegramUserId).WillCascadeOnDelete(false);


            modelBuilder.Entity<TelegramUser>()
                .HasMany(t => t.TelegramPosts)
                .WithOptional(t => t.TelegramUser)
                .HasForeignKey(t => t.TelegramUserId).WillCascadeOnDelete(false);







            modelBuilder.Entity<SocialChannel>().HasMany(t => t.Feeds)
                .WithOptional(t => t.SocialChannel)
                .HasForeignKey(t => t.SocialChannelId).WillCascadeOnDelete(false);


            modelBuilder.Entity<TelegramUser>()
                .HasMany(t => t.Feeds)
                .WithOptional(t => t.TelegramUser)
                .HasForeignKey(t => t.TelegramUserId).WillCascadeOnDelete(false);




            modelBuilder.Entity<Feed>().HasMany(t => t.TelegramPosts)
                .WithOptional(t => t.Feed)
                .HasForeignKey(t => t.FeedId).WillCascadeOnDelete(false);

            
                 
            modelBuilder.Entity<UserAccount>()
                .HasMany(t => t.TelegramUsers)
                .WithOptional(t => t.UserAccount)
                .HasForeignKey(t => t.UserAccountId).WillCascadeOnDelete(false);

            
            modelBuilder.Entity<SocialChannel>()
                .HasMany(t => t.WebApiPosts)
                .WithRequired(t => t.SocialChannel)
                .HasForeignKey(t => t.SocialChannelId).WillCascadeOnDelete(false);


            /*modelBuilder.Entity<TelegramUser>().HasMany(t => t.InstagramChannels)
                .WithRequired(t => t.TelegramUser)
                .HasForeignKey(t => t.TelegramUserId);*/
        }
    }
}