﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TelegramBotsWebApplication.Models
{
    public class Log
    {
        public Log()
        {
            Date=DateTime.Now;
        }
        [Key]
        public int Id { get; set; }

        public string Exception { get; set; }
        public DateTime Date { get; set; }
    }
}