﻿using System.ComponentModel.DataAnnotations;

namespace TelegramBotsWebApplication.Models
{
    public class TemporaryData
    {
        [Key] public int Id { get; set; }

        public int TelegramUserId { get; set; }
        public  TelegramUser TelegramUser { get; set; }
        public int ChannelId { get; set; }
    }
}