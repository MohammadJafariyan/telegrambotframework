﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Telegram.Bot.Types.Enums;
using TelegramBotsWebApplication.Areas.Admin.Service;

namespace TelegramBotsWebApplication.Models
{
    /// <summary>
    /// کانال های تلگرام هم مقصد و هم مبدا
    /// </summary>
    public class SocialChannel:IEntity
    {

        public SocialChannel()
        {
            Feeds = new List<Feed>();
        }
        [Key] public int Id { get; set; }

        /// <summary>
        /// کد مخصوص کانال تلگرام
        /// </summary>
        public long ChatId { get; set; } 

        /// <summary>
        /// آدرس آن معمولا بصورت url
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// هر کاربر تلگرامی ثبت نام کننده از بات تلگرام
        /// هر کاربری که استارت بزند با پیامی بفرستد ذخیره می گردد
        /// </summary>
        public TelegramUser TelegramUser { get; set; }

        /// <summary>
        ///  کد کاربر
        /// </summary>
        public int TelegramUserId { get; set; }


        /// <summary>
        /// نوع کانال مبدا یا مقصد 
        /// </summary>
        public ChannelType ChannelType { get; set; }

        /// <summary>
        /// نوع این رکورد تلگرام است یا اینستاگرام
        /// </summary>
        public SocialChannelType SocialChannelType { get; set; }

        /// <summary>
        /// فید های مخصوص این کانال
        /// </summary>
        public List<Feed> Feeds { get; set; }

        /// <summary>
        /// عنوان چت
        /// </summary>
        public string ChatTitle { get; set; }

        /// <summary>
        ///  نوع چت مانند عمومی یا خصوصی کانال یا گروه
        /// </summary>
        public ChatType ChatType { get; set; }


        /// <summary>
        /// لینک دعوت
        /// </summary>
        public string ChatInviteLink { get; set; }

        /// <summary>
        /// پست های وب سایت های شخص ثالث که به این کانال ارسال شده است یا خواهد شد
        /// </summary>

        public List<WebApiPost> WebApiPosts { get; set; }
    }

 
}