﻿using System;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBotsWebApplication.Handlers.ChannelsSave;
using TelegramBotsWebApplication.Models;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication.Handlers.DefineSourceHandlers
{
    public class DefineInstagramPagesHandler : ChannelsSaveHandler
    {
        public override string UniqueName
        {
            get { return "DefineInstagramPagesHandler"; }
        }
        public DefineInstagramPagesHandler()
        {
            this.ChannelType = ChannelType.Source;
            this.SocialChannelType = SocialChannelType.Instagram;
            this.InitialMessage = "نام صفحه اینستاگرام را بصورت @example وارد نمایید";
        }
    }
}