﻿using System;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBotsWebApplication.Handlers.ChannelsSave;
using TelegramBotsWebApplication.Models;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication.Handlers.DefineSourceHandlers
{
    public class DefineTargetTelegramPagesHandler : ChannelsSaveHandler
    {
        
        public override string UniqueName
        {
            get { return "DefineTargetTelegramPagesHandler"; }
        }
        public DefineTargetTelegramPagesHandler()
        {
            ChannelType= ChannelType.Source;
            SocialChannelType= SocialChannelType.Telegram;
        }
    }
}