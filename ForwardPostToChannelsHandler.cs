﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBotsWebApplication.Models;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication.Handlers
{
    public class ForwardPostToChannelsHandler : IMenuHandler
    {
        private readonly TelegramPostService _telegramPostService = new TelegramPostService();

        public async void BotOnMessageReceived(object sender, MessageEventArgs e)
        {
            var message = e.Message;
            //   if (message == null || message.Type != MessageType.Text) return;

            var currUser = CurrentRequestHolderSingleton.CurrentRequest.User;

            if (message.ForwardDate != null)
            {
                TelegramPost tpost = TelegramPost.GetPostFromMessage(message, currUser.Id);

                _telegramPostService.Save(tpost);


                await ShowHelp(currUser.Id);
                return;
            }


            if (e.Message.Text == "/SendNow")
            {
                await _telegramPostService.SetRecentForwardedPostsSendTime(isSendNow: true, currUserId: currUser.Id);

                await PostRecentPosts(currUser.Id);
                await ShowSuccessTimeEntry();

                // به منزله بازگشت به منو اصلی
                await CurrentRequestHolderSingleton.CurrentRequest.SaveState(null);
                new HelloHandler().ShowInitialMenu();
                return;
            }

            if (e.Message.Text == "/CancelSend")
            {
                // به منزله بازگشت به منو اصلی
                await CurrentRequestHolderSingleton.CurrentRequest.SaveState(null);
                new HelloHandler().ShowInitialMenu();
                return;
            }

            Regex regex = new Regex("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$");
            string text = e.Message.Text ?? "";

            if (e.Message.Type == MessageType.Text)
                if (text.Length < 10 || regex.IsMatch(text))
                {
                    try
                    {
                        var arr = e.Message.Text.Split(':');

                        int hour = int.Parse(arr[0]);
                        int minute = int.Parse(arr[1]);

                        if (hour < 0 || hour > 23)
                        {
                            await ShowErrorTimeEntry();
                        }

                        if (minute < 0 || minute > 59)
                        {
                            await ShowErrorTimeEntry();
                        }


                        await _telegramPostService.SetRecentForwardedPostsSendTime(hour, minute, currUser.Id);

                        await ShowSuccessTimeEntry(hour, minute);

                        // به منزله بازگشت به منو اصلی
                        await CurrentRequestHolderSingleton.CurrentRequest.SaveState(null);
                        new HelloHandler().ShowInitialMenu();
                        return;
                    }
                    catch (Exception exception)
                    {
                        await ShowHelp(currUser.Id);
                    }

                    return;
                }


            #region addPost

            {
                TelegramPost tpost = TelegramPost.GetPostFromMessage(message, currUser.Id);

                _telegramPostService.Save(tpost);
            }

            #endregion


            await ShowHelp(currUser.Id);
            return;
        }

        private async Task PostRecentPosts(int currUserId)
        {
            //await TelegramCallerService.PostToChannels(_telegramPostService.GetRecentAddedNotSentPostsByTelegramUserIdForSend(currUserId));
            var forwardPosts = _telegramPostService.GetRecentAddedNotSentPostsByTelegramUserIdForSend(currUserId);

            try
            {
                var channelIds = await new ForwardPostCallerService().SendForwardPosts(forwardPosts, currUserId);
                _telegramPostService.SetSentForForwardPosts(forwardPosts, channelIds);
            }
            catch (Exception e)
            {
                try
                {
                    await Bot.Api.SendTextMessageAsync(
                        chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                        text: MyGlobal.RecursiveExecptionMsg(e)
                    );
                }
                catch (Exception exception)
                {
                    //todo:must be logged
                }
            }
        }


        public static async Task PostRecentPosts(DateTime now)
        {
            var _telegramPostService=new TelegramPostService();
            //await TelegramCallerService.PostToChannels(_telegramPostService.GetRecentAddedNotSentPostsByTelegramUserIdForSend(currUserId));
            var forwardPosts =await _telegramPostService.GetAllUsersNotSendAsync(now,now.AddMinutes(-5));

            try
            {
                foreach (var forwardPost in forwardPosts)
                {
                    if (forwardPost.TelegramUserId.HasValue==false)
                    {
                        throw new Exception("کد کاربری صفر است ");
                    }
                    var channelIds = await new ForwardPostCallerService()
                        .SendForwardPosts(forwardPosts, forwardPost.TelegramUserId.Value);

                    _telegramPostService.SetSentForForwardPosts(forwardPosts, channelIds);

                }
            }
            catch (Exception e)
            {
                try
                {
                    await Bot.Api.SendTextMessageAsync(
                        chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                        text: MyGlobal.RecursiveExecptionMsg(e)
                    );
                }
                catch (Exception exception)
                {
                    //todo:must be logged
                }
            }
        }

        private async Task ShowSuccessTimeEntry(int hour, int minute)
        {
            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text: $@"زمان ارسال دریافت گردید : ساعت {hour} و {minute} دقیقه ");
        }

        private async Task ShowSuccessTimeEntry()
        {
            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text: $@"زمان ارسال دریافت گردید : الان ");
        }

        private async Task ShowErrorTimeEntry()
        {
            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text: "زمان اشتباه است  " +
                      "\n" +
                      "ساعت مشخص : \r\nبرای تعیین زمان ارسال بصورت زیر ارسال نمایید ؛\r\nدقیقه : ساعت \r\nبرای مثال : 13:03 \r\n"
            );
        }

        private async Task ShowHelp(int currUserId)
        {
            var postsByTelegramUser = _telegramPostService.GetRecentAddedNotSentPostsByTelegramUserId(currUserId);

            int count = postsByTelegramUser.Count();
            string m1 = "به تعداد";
            string m2 = "پست دریافت گردید";

            string desc = count > 0
                ? "\n" +
                  @"چه زمانی میخواهید به کانال ها ارسال شود ؟ "
                  + "\n" +
                  "الان ارسال کند: "
                  + "\n" +
                  "/SendNow"
                  + "\n" +
                  "ساعت مشخص : "
                  + "\n" +
                  "برای تعیین زمان ارسال بصورت زیر ارسال نمایید ؛"
                  + "\n" +
                  "دقیقه : ساعت "
                  + "\n" +
                  "برای مثال : 13:03 "
                  + "\n" +
                  "لغو ، نمی فرستم : "
                  + "\n" +
                  "/CancelSend"
                : "";

            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text: $@"{m1} {count} {m2}"
                      + desc
            );
        }

        public void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs e)
        {
        }

        public void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs e)
        {
        }

        public void BotOnReceiveError(object sender, ReceiveErrorEventArgs e)
        {
        }

        public void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs e)
        {
        }

        public void ShowInitialMenu(string msg)
        {
        }

        public string UniqueName
        {
            get { return nameof(ForwardPostToChannelsHandler); }
        }
    }
}