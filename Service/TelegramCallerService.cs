﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Ajax.Utilities;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    public class TelegramCallerService : BaseCallerService
    {
        public TelegramCallerService(Feed feed, BotDbContext context) : base(feed, context)
        {
        }
        
        
        public override async Task<List<TelegramPost>> Call()
        {
            try
            {
                List<TelegramPost> list = new List<TelegramPost>();
                var updates = await Bot.Api.GetUpdatesAsync(-1, 10);
                foreach (var update in updates)
                {
                    var post=new TelegramPost();
                    post.PushToTelegramMessagePostIds( new TelegramMessagePostViewModel
                    {
                        MessageId = update.ChannelPost.MessageId,
                        ChannelId = update.ChannelPost.Chat.Id
                    });
                    post.Title = update.ChannelPost.Caption;
                    post.Summary = update.ChannelPost.Text;
                    post.Summary = update.ChannelPost.Text;
                    post.TelegramFileId = update.ChannelPost.Photo?[0].FileId;
                    post.TelegramMessageType = update.Message.Type;
                    
                    
                    list.Add(post);
                }

                this._Posts = list;
                return list;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public static async Task PostToChannels(List<TelegramPost> posts)
        {
            List<int> showedList = new List<int>();
            var socialChannels = posts.SelectMany(p => p.TelegramUser.TelegramChannels).ToList();

            socialChannels= socialChannels.DistinctBy(s=>s.ChatId).ToList();
            foreach (var channel in socialChannels)
            {
                foreach (var post in posts)
                {
                    try
                    {
                        await Bot.Api.SendTextMessageAsync(
                            chatId: channel.ChatId,
                            text: post.Text);
                    }
                    catch (Exception e)
                    {
                        // فقط یک بار پیغام دهد
                        if (showedList.Any(s=>s==channel.Id))
                        {
                        }
                        else
                        {

                            string msg = showedList.Count==0 ? "پست ارسال نشد ، جهت ارسال به کانال زیر ابتدا باید این ربات را در کانال یا گروه خود عضو کرده و آن را ادمین نمایید"+ "\n" :"";
                                         //todo: must logged
                                         await Bot.Api.SendTextMessageAsync(
                                             chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                                             text: msg +
                                                   channel.ChatTitle);
                            showedList.Add(channel.Id);
                        }

                     
                    }
                   
                }
            }
        }

       
    }
}