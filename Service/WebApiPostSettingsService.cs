﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    public class WebApiPostSettingsService
    {
        public static WebApiPostSettings GetSetting()
        {
            using (var db=new BotDbContext())
            {

                // اگر قبلا تعریف نشده باشد یعنی کل روز
                var res= db.WebApiPostSettings.FirstOrDefault();
                if (res==null)
                {
                    return new WebApiPostSettings
                    {
                        From = new DateTime(2020, 1, 1, 0, 0, 0),
                        To = new DateTime(2020, 1, 1, 0, 0, 0),
                    };
                }

                return res;
            }
        }

        public static void SaveSetting(WebApiPostSettings record)
        {
            using (var db = new BotDbContext())
            {
                //فقط یک رکورد نگه می داریم
                var exist = db.WebApiPostSettings.FirstOrDefault();
                if (exist!=null)
                {
                    // اگر قبلا بود آن را تغییر می دهد
                    exist.To = record.To;
                    exist.From = record.From;
                    db.Entry(exist).State = EntityState.Modified;
                }
                else
                {
                    // فقط یکی باید باشد
                    db.WebApiPostSettings.Add(record);
                }

                db.SaveChanges();
            }
        }
    }
}