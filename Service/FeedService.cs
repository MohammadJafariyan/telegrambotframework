﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Threading.Tasks;
using System.Xml;
using InstagramWrapper;
using TelegramBotsWebApplication.Handlers;
using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    
    public class FeedService : BaseService<Feed>
    {
        
        /// <summary>
        /// بررسی میکند آنچه کاربر وارد نموده است درست باشد
        /// </summary>
        /// <param name="type"></param>
        /// <param name="address"></param>
        /// <param name="telegramUserId"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static async Task<long> DetermineCorrectness(FeedType type, string address,
            int telegramUserId)
        {
            if (type == FeedType.WebRss)
            {
                try
                {
                    await BaseCallerService.ReadRSS(address);
                    return -1;
                }
                catch (Exception e)
                {
                    throw new Exception("آدرس سایت فید صحیح نبوده و قابل خوانش نیست");
                }
            }

            if (type == FeedType.Telegram)
            {
            return  await  HelloHandler.IsChannelCorrectForBeingSource(address, telegramUserId);
            }

            if (type == FeedType.Instagram)
            {
                var userId = MyGlobal.SplitAndGetRest(address, "instagram.com/");

                var _instagramService = new InstagramService();
                try
                {
                    if (_instagramService.GetUserInfo(userId).Data == null)
                    {
                        throw new Exception("صفحه اینستاگرامی یافت نشد");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw new Exception("صفحه اینستاگرامی یافت نشد");
                }
            }
            return -1;

        }

     

        public static FeedType DetermineFeedType(string address)
        {
            if (string.IsNullOrEmpty(address))
            {
                throw new Exception("آدرس فید ارسالی خالی است");
            }


            if (address.Contains("t.me/"))
            {
                return FeedType.Telegram;
            }

            if (address.Contains("instagram.com/"))
            {
                return FeedType.Instagram;
            }

            if (address.Contains("#"))
            {
                return FeedType.Hashtag;
            }

            //آیا ادرس اینترنتی است
            Uri uriResult;
            bool result =Uri.TryCreate(address, UriKind.Absolute, out uriResult)
                          && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            if (result)
            {
                return FeedType.WebRss;
            }


            throw new Exception("ادرس وارد شده صحیح نیست");
        }

        public static Feed StrsToExchange( Feed feed)
        {
            if (string.IsNullOrEmpty(feed.ExchangeWordsFrom))
            {
                return feed;
            }

            var froms= feed.ExchangeWordsFrom.Split(',');
            var tos= feed.ExchangeWordsTo.Split(',');

            if (froms.Length!=tos.Length)
            {
                throw new Exception("lenght not equal");
            }

            List<ExchaneArrayForFeed> exchaneArrayForFeeds = new List<ExchaneArrayForFeed>();
            for (int i = 0; i < froms.Length; i++)
            {
                exchaneArrayForFeeds.Add(new ExchaneArrayForFeed
                {
                    FromWord = froms[i],
                    ToWord = tos[i]
                });
            }

            feed.ExchaneArrayForFeed = exchaneArrayForFeeds;
            return feed;

        }
        public static void ExchangeToStrs(ref Feed feed)
        {
            if (feed.ExchaneArrayForFeed==null)
            {
                return;
            }

            feed.ExchaneArrayForFeed=feed.ExchaneArrayForFeed.Where(e => !string.IsNullOrEmpty(e.FromWord)
                                                                         && !string.IsNullOrEmpty(e.ToWord)).ToList();

            var froms= feed.ExchaneArrayForFeed.Select(e => e.FromWord ?? "").ToArray();
            var tos= feed.ExchaneArrayForFeed.Select(e => e.ToWord ?? "").ToArray();

            if (froms.Length!=tos.Length)
            {
                throw new Exception("lenght not equal");
            }

            feed.ExchangeWordsFrom= string.Join(",", froms);
            feed.ExchangeWordsTo= string.Join(",", tos);
        }
    }
}