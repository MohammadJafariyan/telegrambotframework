﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TelegramBotsWebApplication.Handlers;
using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    public class WepApiPostCallerService 
    {
        public async Task PostAllAsyc(IQueryable<WebApiPost> webApiPosts)
        {
            foreach (var webApiPost in webApiPosts)
            {

                /// اگر خطایی در ارسال یکی از پست ها بود رد می شود و موجب استوپ کل حلقه نمی شود
                try
                {
                    await TelegramGlobalApiHandler.SendPostToChannel(webApiPost.SocialChannel.ChatId, webApiPost);
                  
                    Thread.Sleep(1000);
                }
                catch (Exception e)
                {
                    MyGlobal.Log(e);
                }
            }
        }

        internal async Task DeleteAll(IQueryable<WebApiPost> webApiPosts)
        {

            foreach (var webApiPost in webApiPosts)
            {
                /// اگر خطایی در حذف یکی از پست ها بود رد می شود و موجب استوپ کل حلقه نمی شود
                try
                {
                    await TelegramGlobalApiHandler.DeletePostFromChannel(webApiPost.SocialChannel.ChatId, webApiPost);

                    Thread.Sleep(1000);
                }
                catch (Exception e)
                {
                MyGlobal.Log(e);
                }
            }
        }
    }
}