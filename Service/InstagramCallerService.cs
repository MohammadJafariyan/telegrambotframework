﻿using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    /// <summary>
    /// این کلاس برای فراخوانی از اینستاگرام است
    /// در حال حاضر بدلیل کار نکردن api اینستاگرام و دادن متد های جدید استفاده نشده است
    /// </summary>
    public class InstagramCallerService : BaseCallerService
    {
        public InstagramCallerService(Feed feed, BotDbContext context) : base(feed, context)
        {
        }
    }
}