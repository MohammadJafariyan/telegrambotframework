﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Telegram.Bot.Types.InputFiles;
using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    
    /// <summary>
    /// این کلاس تنظیمات تعریف شده در لینک -> تنظیمات فید
    /// را اعمال میکند
    /// </summary>
    public class FeedSettingApplierService
    {
        public TelegramPost ApplySetting(TelegramPost post, Feed feed)
        {
            if (!string.IsNullOrEmpty(feed.ExchangeWordsFrom)
                && !string.IsNullOrEmpty(feed.ExchangeWordsTo))
            {
                var fromArr = feed.ExchangeWordsFrom?.Split(',');
                var toArr = feed.ExchangeWordsTo?.Split(',');

                if (fromArr?.Length != toArr?.Length)
                {
                    throw new Exception("fromArr.Length!=toArr.Length");
                }

                for (int i = 0; i < fromArr?.Length; i++)
                {
                    post.Summary = post.Summary?.Replace(fromArr[i], toArr[i]);
                    post.Title = post.Title?.Replace(fromArr[i], toArr[i]);
                    post.Caption = post.Caption?.Replace(fromArr[i], toArr[i]);
                    post.Text = post.Text?.Replace(fromArr[i], toArr[i]);
                }
            }

            if (feed.IncludeArticleLink && !string.IsNullOrEmpty(post.RssLink))
            {
                post.Summary += '\n' + post.RssLink;
            }


            if (feed.DeleteHashtagSigns)
            {
                post.Summary = string.IsNullOrEmpty(post.Summary) ?"":  string.Join(" ", post.Summary?.Split(' ').Where(s => !s.Contains("#")).ToArray());
                post.Title = string.IsNullOrEmpty(post.Title) ? "" : string.Join(" ", post.Title?.Split(' ').Where(s => !s.Contains("#")).ToArray());
                post.Caption = string.IsNullOrEmpty(post.Caption) ? "" : string.Join(" ", post.Caption?.Split(' ').Where(s => !s.Contains("#")).ToArray());
                post.Text = string.IsNullOrEmpty(post.Text) ? "" : string.Join(" ", post.Text?.Split(' ').Where(s => !s.Contains("#")).ToArray());
            }

            if (feed.DeleteHashtagFromEnd)
            {
                post.Summary = post.Summary?.Replace("#", "");
                post.Title = post.Title?.Replace("#", "");
                post.Caption = post.Caption?.Replace("#", "");
                post.Text = post.Text?.Replace("#", "");
            }

            if (!string.IsNullOrEmpty(feed.SignEndOfPost))
            {
                if (string.IsNullOrEmpty(post.Summary))
                {
                    post.Summary = "";
                }
                post.Summary += "\n" + feed.SignEndOfPost;
            }
            
            if (feed.WordCountLimit.HasValue && feed.WordCountLimit>0)
            {
                if (post?.Summary?.Length > feed.WordCountLimit)
                {
                    post.Summary = post.Summary.Substring(0, feed.WordCountLimit.Value);
                }
                if (post?.Caption?.Length > feed.WordCountLimit)
                {
                    post.Caption = post.Caption.Substring(0, feed.WordCountLimit.Value);
                }
                if (post?.Text?.Length > feed.WordCountLimit)
                {
                    post.Text = post.Text.Substring(0, feed.WordCountLimit.Value);
                }
            }

            return post;
        }

        public bool CheckWordsMustBeInside(Feed feed, TelegramPost post)
        {
            if (string.IsNullOrEmpty(feed.WordsMustBeInside))
            {
                return true;
            }

            var arr = feed.WordsMustBeInside.Split(',');
            for (int i = 0; i < arr.Length; i++)
            {
                var contains = post.Summary?.Contains(arr[i]);
                var titlecontains = post.Title?.Contains(arr[i]);
                var Captioncontains = post.Caption?.Contains(arr[i]);
                var Textcontains = post.Text?.Contains(arr[i]);

                if (contains == false && titlecontains == false
                                      && Captioncontains == false
                                      && Textcontains == false)
                {
                    return false;
                }
                else
                {


                    return true;
                }
            }

            return true;
        }

        public bool CheckOkeyForExcludeWords(Feed feed, TelegramPost post)
        {
            if (string.IsNullOrEmpty(feed.WordsNotMustBeInside))
            {
                return true;
            }

            var arr = feed.WordsNotMustBeInside.Split(',');
            for (int i = 0; i < arr.Length; i++)
            {

                var contains = post.Summary?.Contains(arr[i]);
                var titlecontains = post.Title?.Contains(arr[i]);
                var Captioncontains = post.Caption?.Contains(arr[i]);
                var Textcontains = post.Text?.Contains(arr[i]);

                if (contains == true || titlecontains == true
                                     || Captioncontains == true
                                     || Textcontains == true)
                {
                    return false;
                }
            }

            return true;
        }

       
    }

    public class FeedSettingApplierServiceTests
    {
        FeedSettingApplierService service = new FeedSettingApplierService();
        private TelegramPost post;
        private Feed feed;

        [Test]
        public void TestApplySetting()
        {
            feed = new Feed
            {
                ExchangeWordsFrom = "شست و شوی,نیرو",
                ExchangeWordsTo = "آب پاشی,خیار",
                IncludeArticleLink = true,
                DeleteHashtagSigns = true,
            };

            post = new TelegramPost()
            {
                Title = "ببینید | ضدعفونی خیابان‌های شهر قم توسط نیروهای حشدالشعبی",
                Summary =
                    "خبرگزاری تسنیم با انتشار گزارشی تصویری از شست و شوی خیابان های قم توسط نیروهای حشدالشعبی #خبر داده است",
                ImageUrl = "blob:https://web.telegram.org/7ae001cc-2e0b-475b-aa67-a02e1222d7ff",
                RssLink = "blob:https://web.telegram.org/7ae001cc-2e0b-475b-aa67-a02e1222d7ff",
            };

            post = service.ApplySetting(post, feed);


            Assert.True(post.Summary.Contains(
                "خبرگزاری تسنیم با انتشار گزارشی تصویری از آب پاشی خیابان های قم توسط خیارهای حشدالشعبی داده است"));

            Assert.True(post.Summary.Contains("blob:https://web.telegram.org/7ae001cc-2e0b-475b-aa67-a02e1222d7ff"));


            post = new TelegramPost()
            {
                Title = "ببینید | ضدعفونی خیابان‌های شهر قم توسط نیروهای حشدالشعبی",
                Summary =
                    "خبرگزاری تسنیم با انتشار گزارشی تصویری از شست و شوی خیابان های قم توسط نیروهای حشدالشعبی #خبر داده است",
                ImageUrl = "blob:https://web.telegram.org/7ae001cc-2e0b-475b-aa67-a02e1222d7ff",
                RssLink = "blob:https://web.telegram.org/7ae001cc-2e0b-475b-aa67-a02e1222d7ff",
            };

            Assert.True(post.Summary.Contains(
                "#خبر"));
            feed.DeleteHashtagFromEnd = true;
            post = service.ApplySetting(post, feed);

            Assert.False(post.Summary.Contains(
                "#خبر"));
        }

        [Test]
        public void TestMustInclude()
        {
            feed = new Feed
            {
                ExchangeWordsFrom = "شست و شوی,نیرو",
                ExchangeWordsTo = "آب پاشی,خیار",
                IncludeArticleLink = true,
                DeleteHashtagSigns = true,
                WordsMustBeInside = "خبر"
            };

            post = new TelegramPost()
            {
                Title = "ببینید | ضدعفونی خیابان‌های شهر قم توسط نیروهای حشدالشعبی",
                Summary =
                    "خبرگزاری تسنیم با انتشار گزارشی تصویری از شست و شوی خیابان های قم توسط نیروهای حشدالشعبی #خبر داده است",
                ImageUrl = "blob:https://web.telegram.org/7ae001cc-2e0b-475b-aa67-a02e1222d7ff",
                RssLink = "blob:https://web.telegram.org/7ae001cc-2e0b-475b-aa67-a02e1222d7ff",
            };

            bool istrue = service.CheckWordsMustBeInside(feed, post);


            Assert.True(istrue);
        }

        [Test]
        public void TestMustexclude()
        {
            feed = new Feed
            {
                ExchangeWordsFrom = "شست و شوی,نیرو",
                ExchangeWordsTo = "آب پاشی,خیار",
                IncludeArticleLink = true,
                DeleteHashtagSigns = true,
                WordsNotMustBeInside = "خبر"
            };

            post = new TelegramPost()
            {
                Title = "ببینید | ضدعفونی خیابان‌های شهر قم توسط نیروهای حشدالشعبی",
                Summary =
                    "خبرگزاری تسنیم با انتشار گزارشی تصویری از شست و شوی خیابان های قم توسط نیروهای حشدالشعبی #خبر داده است",
                ImageUrl = "blob:https://web.telegram.org/7ae001cc-2e0b-475b-aa67-a02e1222d7ff",
                RssLink = "blob:https://web.telegram.org/7ae001cc-2e0b-475b-aa67-a02e1222d7ff",
            };

            bool istrue = service.CheckOkeyForExcludeWords(feed, post);


            Assert.True(istrue);
        }

        
        [Test]
        public void TestLImitTest()
        {
            feed = new Feed
            {
                ExchangeWordsFrom = "شست و شوی,نیرو",
                ExchangeWordsTo = "آب پاشی,خیار",
                IncludeArticleLink = true,
                DeleteHashtagSigns = true,
                WordsNotMustBeInside = "خبر",
                WordCountLimit = 10
            };

            post = new TelegramPost()
            {
                Title = "ببینید | ضدعفونی خیابان‌های شهر قم توسط نیروهای حشدالشعبی",
                Summary =
                    "خبرگزاری تسنیم با انتشار گزارشی تصویری از شست و شوی خیابان های قم توسط نیروهای حشدالشعبی #خبر داده است",
                ImageUrl = "blob:https://web.telegram.org/7ae001cc-2e0b-475b-aa67-a02e1222d7ff",
                RssLink = "blob:https://web.telegram.org/7ae001cc-2e0b-475b-aa67-a02e1222d7ff",
            };

             post=service.ApplySetting( post,feed);


            Assert.AreEqual(post.Summary.Length,feed.WordCountLimit);
        }


        /*[Test]
        public async Task SendByFeedSettingTest()
        {
            feed = new Feed
            {
                ExchangeWordsFrom = "شست و شوی,نیرو",
                ExchangeWordsTo = "آب پاشی,خیار",
                IncludeArticleLink = true,
                DeleteHashtagSigns = true,
                WordsNotMustBeInside = "خبر"
            };

            post = new TelegramPost()
            {
                Title = "ببینید | ضدعفونی خیابان‌های شهر قم توسط نیروهای حشدالشعبی",
                Summary =
                    "خبرگزاری تسنیم با انتشار گزارشی تصویری از شست و شوی خیابان های قم توسط نیروهای حشدالشعبی #خبر داده است",
                ImageUrl = "blob:https://web.telegram.org/7ae001cc-2e0b-475b-aa67-a02e1222d7ff",
                RssLink = "blob:https://web.telegram.org/7ae001cc-2e0b-475b-aa67-a02e1222d7ff",
            };

           await  service.SendBySetting(feed, post);


        }*/
        
        
    }
}