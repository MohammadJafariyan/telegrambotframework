﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using NUnit.Framework;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    public class WebRssCallerService : BaseCallerService
    {

        public async override Task<List<TelegramPost>> Call()
        {
            try
            {
                /*
                var htmlContentAsString= ReadAllContentURLAsString(_feed.Address);
                
                
                StringWriter myWriter = new StringWriter();

                string myEncodedString = HttpUtility.HtmlEncode(htmlContentAsString,myWriter);
                */


                SyndicationFeed feed =await BaseCallerService.ReadRSS(_feed.Address);
                

                List<TelegramPost> posts = new List<TelegramPost>();
                foreach (SyndicationItem item in feed.Items)
                {
                    
                  var  _post=new TelegramPost();
                    //_post.ImageUrl = feed.ImageUrl.AbsolutePath;

                    
                    foreach (SyndicationLink enclosure in item.Links.Where<SyndicationLink>(x => x.RelationshipType == "enclosure"))
                    {
                        _post.ImageUrl             = enclosure.Uri.AbsoluteUri;
                        _post.Imagelength         = enclosure.Length;
                        _post.MediaType    = enclosure.MediaType;
                    }
                    _post.Title = item.Title.Text;
                    _post.Summary = item.Summary.Text;
                    _post.RssLink = string.Join(",",item.Links);
                    _post.UniqId = item.Id;

                    _post.Feed = _feed;
                    posts.Add(_post);
                   
                }

                this._Posts = posts;
                return posts;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        
        private async Task<Stream> ReadAllContentURLAsStream(string feedAddress)
        {
            var httpClient = new HttpClient();
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            var httpResponseMessage = await httpClient.GetAsync(feedAddress);
            return await httpResponseMessage.Content.ReadAsStreamAsync();
        }


        public WebRssCallerService(Feed feed, BotDbContext context) : base(feed, context)
        {
        }
        public static string RemoveTroublesomeCharacters(string inString)
        {
            if (inString == null) return null;

            StringBuilder newString = new StringBuilder();
            char ch;

            for (int i = 0; i < inString.Length; i++)
            {

                ch = inString[i];
                // remove any characters outside the valid UTF-8 range as well as all control characters
                // except tabs and new lines
                if ((ch < 0x00FD && ch > 0x001F) || ch == '\t' || ch == '\n' || ch == '\r')
                {
                    newString.Append(ch);
                }
            }
            return newString.ToString();

        }
    }

    public class WebRssCallerServiceTests
    {
        private WebRssCallerService _service = new WebRssCallerService(null,null);
        [Test]
        public async Task CallTests()
        {
            await BaseCallerService.ReadRSS("https://www.yjc.ir/fa/rss/allnews");
            await BaseCallerService.ReadRSS("http://khabaronline.ir/rss/tp/83");
            await BaseCallerService.ReadRSS("https://www.bartarinha.ir/fa/rss/allnews");
            await BaseCallerService.ReadRSS("http://rss.cnn.com/rss/cnn_topstories.rss");
        }
    }

    internal class StreamUtil
    {
        internal static byte[] ReadToEnd(System.IO.Stream stream)
        {
            byte[] readBuffer = new byte[4096];

            int totalBytesRead = 0;
            int bytesRead;

            while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
            {
                totalBytesRead += bytesRead;

                if (totalBytesRead == readBuffer.Length)
                {
                    int nextByte = stream.ReadByte();
                    if (nextByte != -1)
                    {
                        byte[] temp = new byte[readBuffer.Length * 2];
                        Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                        Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                        readBuffer = temp;
                        totalBytesRead++;
                    }
                }
            }

            byte[] buffer = readBuffer;
            if (readBuffer.Length != totalBytesRead)
            {
                buffer = new byte[totalBytesRead];
                Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
            }
            return buffer;
        }
    }
}