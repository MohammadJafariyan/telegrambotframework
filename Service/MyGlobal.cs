﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using Telegram.Bot.Types;
using TelegramBotsWebApplication.Handlers;
using TelegramBotsWebApplication.Models;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication
{
    public class MyGlobal
    {
        public static string RecursiveExecptionMsg(Exception e)
        {
            string msg = null;
            Exception e2 = e;
            while (e2 != null)
            {
                msg += e2.Message;
                e2 = e2.InnerException;
            }

            return msg;
        }

        public static string ExtractUniqueNameForHandler(string callbackQueryData)
        {
            return callbackQueryData.Split('_')[0];
        }

        public static string ExtractValueInlineQuery(string idstr)
        {
            return idstr.Split('_')[1];
        }

        public static DateTime CreateDateFromTime(int year, int month, int day, DateTime time)
        {
            return new DateTime(year, month, day, time.Hour, time.Minute, 0);
        }

        public static int ValidateHash(string hash)
        {
            string userIdstr = EncryptionHelper.Decrypt(hash);

            userIdstr = userIdstr.Split('_')[0];

            int userId = int.Parse(userIdstr);
            return userId;
        }

        public static string Encrypt(string txt)
        {
            var now = txt + "_" + DateTime.Now;
            return EncryptionHelper.Encrypt(now);
        }


        public static string SplitAndGetRest(string str, string tosub)
        {
            var i = str.IndexOf(tosub, StringComparison.CurrentCulture);

            var start = i + tosub.Length;
            var length = str.Length - start;
            return str.Substring(start, length);
        }

        public static string GetTelegramChatId(string address)
        {
            return MyGlobal.SplitAndGetRest(address, "t.me/");
        }

        public static void Log(Exception exception)
        {
            try
            {
                using (var db = new BotDbContext())
                {
                    db.Logs.Add(new Log
                    {
                        Exception = MyGlobal.RecursiveExecptionMsg(exception)
                    });
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                // ignored
            }
        }

        public static bool IsUnitTestEnvirement = false;

        public static T Clone<T>(T feed)
        {
            var json = JsonConvert.SerializeObject(feed,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

            return JsonConvert.DeserializeObject<T>(json);
        }

        public static List<long> GetChannelIds(string DeliveredChannelIds)
        {
            if (string.IsNullOrEmpty(DeliveredChannelIds))
            {
                throw new Exception("این پست به هیچ کانال ارسال نشده است");
            }

            var strings = DeliveredChannelIds.Split(',');

            var list = strings.Select(s =>
            {
                if (s.Contains("_"))
                {
                    var tmp=s.Split('_')[1];
                    return long.Parse(tmp);
                }
                else
                {
                    return long.Parse(s);
                }
            }).ToList();
            return list;
        }

        public static string ExtractChannelIds(IQueryable<SocialChannel> socialChannels)
        {
            string channelIds = string.Join(",", socialChannels
                .Select(s => s.ChatTitle + "_" + s.ChatId + "_" + s.ChannelType + "_" + s.Id).ToList());

            return channelIds;
        }
    }

    public class MyJsonResponse<T>
    {
        public MyJsonResponseType Type { get; set; }
        public T Single { get; set; }
        public List<T> List { get; set; }
        public string Message { get; set; }
    }
}