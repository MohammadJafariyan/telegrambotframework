﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using NUnit.Framework;
using TelegramBotsWebApplication.Handlers;
using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    /// <summary>
    /// سرویسی است که به تایمر داده می شود
    /// تا هر 5 دقیقه چک کند و اگر زمان فیدی رسیده باشد آن را فراخانی میکند
    /// </summary>
    public static class TimerService
    {
        public static async void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            try
            {
                using (var db = new BotDbContext())
                {
                    var now = DateTime.Now;


                    // آیا وقت ارسال پست های شخص ثالث فرا رسیده است ؟
                    if (WebApiPostSettingsService.GetSetting().From <= now
                        && WebApiPostSettingsService.GetSetting().To >= now)
                    {
                        //فراخوانی پست های سایت های شخص ثالث
                        var service = new WepApiPostCallerService();

                        var webApiPosts = db.WebApiPosts.Include(w => w.SocialChannel).Where(w =>
                            w.IsSentToChannel == false
                            && w.PostDateTime == now.Date).AsNoTracking().AsQueryable();

                        await service.PostAllAsyc(webApiPosts);
                        // پایان


                        // حذف آن هایی که زمانشان فرا رسیده است
                        var deleteReachedApiPosts = db.WebApiPosts.Include(w => w.SocialChannel)
                            .Where(w => w.ExpireDateTime < now && w.IsDeleted == false).AsNoTracking().AsQueryable();

                        await service.DeleteAll(deleteReachedApiPosts);
                        // پایان
                    }


                    //استخراج فید ها

                    var feedsInThisTime = db.Feeds
                        //  .Where(f => f.TimeFrom < now && now < f.TimeTo)
                        .Include(f => f.SocialChannel)
                        .Include(f => f.SocialChannel.TelegramUser)
                        .Include(f => f.SocialChannel.TelegramUser.UserAccount)
                        .Include(f => f.TelegramPosts)
                        .Where(f => f.SocialChannel.TelegramUser.UserAccount.ExpireDate >= DateTime.Now).ToList();


                    // آن فید هایی که زمانشان فرا رسیده است
                    var reachedTimeFeeds = GetReachedTimeFeeds(now, feedsInThisTime);

                    // ارسال پست های در انتظار ارسال که از طریق ارسال مطلب به کانال ها در بات اضافه شده اند 
                    var postRecentPosts= ForwardPostToChannelsHandler.PostRecentPosts(now);

                    foreach (var feed in reachedTimeFeeds)
                    {
                        try
                        {
                            // هر نوع فیدی دارای کلاس مخصوص خود است
                            var callerService = GetTransferService(feed, db);

                            // فراخانی فید
                            var posts = await callerService.Call();


                            // پست های قدیمی فیلتر می شود
                            var newPosts = await callerService.FilterNewPosts(posts);


                            // ارسال به کانال ها
                            await callerService.PostToChannel(newPosts);


                            // ذخیره پست ها برای فیلتر کردن بعدی
                            await callerService.SavePosts();

                           
                        }
                        catch (Exception exception)
                        {
                            MyGlobal.Log(exception);

                        }


                    
                    }
                    try
                    {
                        // اینجا دیتا را می خواهد تا معطل این عمل نشود
                        await postRecentPosts;
                    }
                    catch (Exception exception)
                    {
                        MyGlobal.Log(exception);
                    }
                }
            }
            catch (Exception exception)
            {
                //لاگ در دیتباس
                MyGlobal.Log(exception);
                Console.WriteLine(exception);
            }
        }


        /// <summary>
        ///                     // آن فید هایی که زمانشان فرا رسیده است
        /// </summary>
        /// <param name="time"></param>
        /// <param name="feeds"></param>
        /// <returns></returns>
        public static List<Feed> GetReachedTimeFeeds(DateTime time, List<Feed> feeds)
        {
            var now = time;
            var fiveMinBefore = time.AddMinutes(-5);

            List<Feed> reachedFeedtime = new List<Feed>();
            foreach (var feed in feeds)
            {
                feed.CheckTimesInADay = feed.CheckTimesInADay <= 0 ? 3 : feed.CheckTimesInADay;

                if (feed.TimeTo <= feed.TimeFrom)
                {
                    continue;
                }

                var everyStepMinutes = (feed.TimeTo.TimeOfDay - feed.TimeFrom.TimeOfDay).TotalMinutes /
                                       feed.CheckTimesInADay;

                var stepTime = feed.TimeFrom;


                while (stepTime <= feed.TimeTo)
                {
                    // آیا وقت یکی از مراحل مابین الان و 5 دقیقه قبل قرار میگیرد ؟ 

                    if (MyGlobal.IsUnitTestEnvirement)
                    {
                        reachedFeedtime.Add(feed);
                        break;
                    }

                    if (fiveMinBefore < stepTime && stepTime <= now)
                    {
                        reachedFeedtime.Add(feed);
                        break;
                    }

                    stepTime = stepTime.AddMinutes(everyStepMinutes);
                }
            }

            MyGlobal.IsUnitTestEnvirement = false;
            return reachedFeedtime;
        }

        public static ICallerService GetTransferService(Feed feed, BotDbContext context)
        {
            switch (feed.Type)
            {
                case FeedType.WebRss:
                    return new WebRssCallerService(feed, context);
                    break;
                case FeedType.Hashtag:
                    return new HashtagCallerService(feed, context);
                    break;
                case FeedType.Instagram:
                    return new InstagramCallerService(feed, context);
                    break;
                case FeedType.Telegram:
                    return new TelegramCallerService(feed, context);
                    break;
            }

            throw new Exception("not Recognized Type");
        }
    }


    public class TimerServiceTests
    {
        [Test]
        public async Task OnTimedEventTest()
        {
            MyGlobal.IsUnitTestEnvirement = true;
            TimerService.OnTimedEvent(null, null);
        }
    }
}