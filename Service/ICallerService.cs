﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    /// <summary>
    /// هر سرویسی که بخواهد فید بخواند
    /// برای خواندن فید از نت
    /// برای خواندن از اینستاگرام
    /// خواندن از هشتگ تلگرام
    /// باید پیاده سازی شود
    /// </summary>
    public interface ICallerService
    {
        Task<List<TelegramPost>> Call();

        Task<List<TelegramPost>> FilterNewPosts(List<TelegramPost> posts);
        Task PostToChannel(List<TelegramPost> posts);
        Task SavePosts();
    }
}