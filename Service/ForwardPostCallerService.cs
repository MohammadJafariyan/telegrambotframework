﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    public class ForwardPostCallerService : BaseCallerService
    {
        private readonly TelegramPostService _telegramPostService = new TelegramPostService();

        public ForwardPostCallerService(Feed feed, BotDbContext context) : base(feed, context)
        {
        }

        public ForwardPostCallerService() : base()
        {
        }

        public async Task ShowForwardPosts(List<TelegramPost> posts,
            long chatId, int currUserId)
        {
            using (var db = new BotDbContext())
            {
                var feed = db.Feeds.FirstOrDefault(f => f.TelegramUserId == currUserId
                                                        && f.Type == FeedType.ForwardPostSetting);


                if (feed == null)
                {
                    feed = new Feed();
                }


                foreach (var telegramPost in posts)
                {
                    feed.SocialChannel = new SocialChannel
                    {
                        ChatId = chatId
                    };
                    this._context = db;
                    this._feed = feed;
                    await this.PostToChannel(new List<TelegramPost>
                    {
                        telegramPost
                    });
                }
            }
        }

        public async Task<List<int>> SendForwardPosts(List<TelegramPost> posts
            , int currUserId)
        {
            using (var db = new BotDbContext())
            {
                var feed = db.Feeds.FirstOrDefault(f => f.TelegramUserId == currUserId
                                                        && f.Type == FeedType.ForwardPostSetting);


                if (feed == null)
                {
                    feed = new Feed();
                }


                var channels = db.SocialChannels
                    .Where(s => s.TelegramUserId == currUserId).ToList();

                if (channels.Any() == false)
                {
                    throw new Exception(
                        "شما هیچ کانالی به ربات اضافه نکرده اید ، ابتدا ربات را به کانال اضافه کرده و آن را ادمین قرار دهید سپس یکی از پیام های کانال یا گروه خود را به اینجا فوروارد نمایید");
                }

                var channelIds = channels.Select(c => c.Id).ToList();

                foreach (var telegramPost in posts)
                {
                    foreach (var socialChannel in channels)
                    {
                        feed.SocialChannel = socialChannel;
                        this._context = db;
                        this._feed = feed;
                        await this.PostToChannel(new List<TelegramPost>
                        {
                            telegramPost
                        });
                    }
                }


                await _telegramPostService.SaveAll(posts);
                return channelIds;
            }
        }

        public async Task DeletePostInChannels(TelegramPost single)
        {
            var channelIds = MyGlobal.GetChannelIds(single.DeliveredChannelIds);

            foreach (var channelId in channelIds)
            {
                try
                {
                    var telegramMessagePostViewModel =
                        single.TelegramMessagePostIds.FirstOrDefault(f => f.ChannelId == channelId);
                    if (telegramMessagePostViewModel == null)
                    {
                        throw new Exception("به این کانال ارسال نشده است");
                    }

                    await Bot.Api.DeleteMessageAsync(channelId, telegramMessagePostViewModel.MessageId);
                    Thread.Sleep(1000);
                }
                catch (Exception e)
                {
                    //ignore
                }
            }
        }

        public async Task EditPostInChannels(TelegramPost single, string text)
        {
            var channelIds = MyGlobal.GetChannelIds(single.DeliveredChannelIds);

            foreach (var channelId in channelIds)
            {
                try
                {
                    var telegramMessagePostViewModel =
                        single.TelegramMessagePostIds.FirstOrDefault(f => f.ChannelId == channelId);
                    if (telegramMessagePostViewModel == null)
                    {
                        throw new Exception("به این کانال ارسال نشده است");
                    }

                    Message message;

                    if (single.TelegramMessageType == MessageType.Text)
                    {
                        message = await Bot.Api.EditMessageTextAsync(channelId, telegramMessagePostViewModel.MessageId,
                            text: text);
                    }
                    else
                    {
                        message = await Bot.Api.EditMessageCaptionAsync(channelId,
                            telegramMessagePostViewModel.MessageId,
                            caption: text);
                    }

                    Thread.Sleep(1000);
                    var update = telegramMessagePostViewModel.Clone();
                    update.MessageId = message.MessageId;
                    single.ReplaceInMessageIds(telegramMessagePostViewModel, update);
                    
                    _telegramPostService.Save(single);
                }
                catch (Exception e)
                {
                    //ignore
                }
            }
        }
    }
}