﻿using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    
    /// <summary>
    ///  بدلیل تغییر api اینستاگرام امکان خواندن وجود نداشت
    /// </summary>
    public class HashtagCallerService : BaseCallerService
    {
        public HashtagCallerService(Feed feed, BotDbContext context) : base(feed, context)
        {
        }
    }
}