﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceModel.Syndication;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    public abstract class BaseCallerService : ICallerService
    {
        protected Feed _feed;
        protected List<TelegramPost> _Posts;

        protected BotDbContext _context;

        protected FeedSettingApplierService _feedSettingApplierService = new FeedSettingApplierService();
        private readonly List<int> showedList = new List<int>();

        public BaseCallerService(Feed feed, BotDbContext context)
        {
            this._feed = feed;
            _context = context;
        }


        public BaseCallerService()
        {
        }

        public static async Task<SyndicationFeed> ReadRSS(string feedAddress)
        {
            System.Net.ServicePointManager.SecurityProtocol =
                SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;


            XmlReader reader;


            try
            {
                // روش اول
                reader = XmlReader.Create(feedAddress);
                SyndicationFeed feed = SyndicationFeed.Load(reader);
                reader.Close();
                return feed;
            }
            catch (Exception e)
            {
                // روش دوم اگر زیپ شده باشد
                var str = await ReadAllContentURLAsString(feedAddress);
                reader = XmlReader.Create(new StringReader(str));
                SyndicationFeed feed = SyndicationFeed.Load(reader);
                reader.Close();
                return feed;
            }
        }

        private static async Task<string> ReadAllContentURLAsString(string feedAddress)
        {
            var httpClient = new HttpClient();
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 |
                                                              SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

            using (var request = new HttpRequestMessage(HttpMethod.Get, new Uri(feedAddress)))
            {
                request.Headers.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml");
                request.Headers.TryAddWithoutValidation("Accept-Encoding", "gzip, deflate");
                request.Headers.TryAddWithoutValidation("User-Agent",
                    "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
                request.Headers.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                using (var ress = await httpClient.SendAsync(request).ConfigureAwait(false))
                {
                    ress.EnsureSuccessStatusCode();
                    using (var responseStream = await ress.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    using (var decompressedStream = new GZipStream(responseStream, CompressionMode.Decompress))
                    using (var streamReader = new StreamReader(decompressedStream))
                    {
                        var response = await streamReader.ReadToEndAsync().ConfigureAwait(false);
                        return response;
                    }
                }
            }


            throw new Exception("خواندن انجام نشد");
        }

        public async virtual Task<List<TelegramPost>> Call()
        {
            return null;
        }

        public virtual Task<List<TelegramPost>> FilterNewPosts(List<TelegramPost> posts)
        {
            foreach (var telegramPost in posts.ToList())
            {
                if (telegramPost.Feed.Type == FeedType.WebRss)
                {
                    //dublicate by feedId
                    if (_feed.TelegramPosts?
                        .Any(feedPost => feedPost.UniqId == telegramPost.UniqId) == true)
                    {
                        posts.Remove(telegramPost);
                    }
                    
                    if (_feed.TelegramPosts?
                        .Any(feedPost => feedPost.Title == telegramPost.Title) == true)
                    {
                        posts.Remove(telegramPost);
                    }
                }

                //todo:telegram
            }

            _Posts = posts;
            return Task.FromResult(posts);
        }

        public virtual async Task PostToChannel(List<TelegramPost> posts)
        {
            for (int i = 0; i < posts.Count; i++)
            {
                try
                {
                    var post = _feedSettingApplierService.ApplySetting(posts[i], _feed);

                    bool isContainsWords = _feedSettingApplierService.CheckWordsMustBeInside(_feed, post);
                    if (isContainsWords == false)
                    {
                        continue;
                    }

                    bool isOk = _feedSettingApplierService.CheckOkeyForExcludeWords(_feed, post);
                    if (isOk == false)
                    {
                        continue;
                    }

                    await SendBySetting(_feed, post);
                    Thread.Sleep(1000);

                }
                catch (Exception e)
                {
                }
            }
        }

        public async Task SendBySetting(Feed feed, TelegramPost post)
        {
            if (feed.IsHasUrlNotSendToChannel)
            {
                if (post.Summary.Contains("http") || post.Summary.Contains("www")
                                                  || post.Summary.Contains(".ir") || post.Summary.Contains(".com"))
                {
                    return;
                }
            }


            try
            {

                Message message = null;

                switch (post.TelegramMessageType)
                {
                    case MessageType.Audio:
                        await Bot.Api.SendAudioAsync(
                            feed.SocialChannel.ChatId,
                            new InputOnlineFile(post.TelegramFileId),
                            (post.Caption ?? "" )+ (post.Text ?? "") + (post.Title ?? "") + "\b" + (post.Summary ?? ""),
                            disableNotification: feed.NotificationDisabled
                        );
                        break;

                    case MessageType.Text:
                        message = await Bot.Api.SendTextMessageAsync(
                            feed.SocialChannel.ChatId,
                            (post.Caption ?? "" )+ (post.Text ?? "") + (post.Title ?? "") + "\b" + (post.Summary ?? ""),
                            disableNotification: feed.NotificationDisabled
                        );
                        break;
                    case MessageType.VideoNote:
                        message = await Bot.Api.SendVideoNoteAsync(
                            feed.SocialChannel.ChatId,
                            new InputOnlineFile(post.TelegramFileId),
                            disableNotification: feed.NotificationDisabled
                        );
                        break;
                    case MessageType.Voice:
                        message = await Bot.Api.SendVoiceAsync(
                            feed.SocialChannel.ChatId,
                            new InputOnlineFile(post.TelegramFileId),
                            (post.Caption ?? "" )+ (post.Text ?? "") + (post.Title ?? "") + "\b" + (post.Summary ?? ""),
                            disableNotification: feed.NotificationDisabled
                        );
                        break;
                    case MessageType.Document:
                        message = await Bot.Api.SendDocumentAsync(
                            feed.SocialChannel.ChatId,
                            new InputOnlineFile(post.TelegramFileId),
                            (post.Caption ?? "" )+ (post.Text ?? "") + (post.Title ?? "") + "\b" + (post.Summary ?? ""),
                            disableNotification: feed.NotificationDisabled
                        );
                        break;
                    case MessageType.Video:
                        message = await Bot.Api.SendVideoAsync(
                            feed.SocialChannel.ChatId,
                            new InputOnlineFile(post.TelegramFileId),
                
                            caption: (post.Caption ?? "") + (post.Text ?? "") + (post.Title ?? "") + "\b" + (post.Summary ?? ""),

                            disableNotification: feed.NotificationDisabled
                        );
                        break;
                    case MessageType.Sticker:
                        message = await Bot.Api.SendStickerAsync(
                            feed.SocialChannel.ChatId,
                            new InputOnlineFile(post.TelegramFileId),
                            disableNotification: feed.NotificationDisabled
                        );
                        break;

                    case MessageType.Photo:
                        string fileId = "";
                        if (!string.IsNullOrEmpty(post.TelegramFileId))
                        {
                            if (post.TelegramFileId.Contains(","))
                            {
                                fileId= post.TelegramFileId.Split(',')[1];
                            }
                            else
                            {
                                fileId = post.TelegramFileId;
                            }
                        }
                        
                        message = await Bot.Api.SendPhotoAsync(
                            feed.SocialChannel.ChatId,
                             string.IsNullOrEmpty(post.ImageUrl) ? new InputOnlineFile(fileId) : new InputOnlineFile(post.ImageUrl),
                            (post.Caption ?? "" )+ (post.Text ?? "") + (post.Title ?? "") + "\b" + (post.Summary ?? ""),
                            disableNotification: feed.NotificationDisabled
                        );
                        break;
                }

                if (message==null)
                {
                    throw new Exception("پیغام بازگشتی نال است");
                }

                post.PushToTelegramMessagePostIds(new TelegramMessagePostViewModel
                {
                     ChannelId = message.Chat.Id,
                     MessageId = message.MessageId
                }); 
            }
            catch (Exception e)
            {
                // فقط یک بار پیغام دهد
                if (showedList.Any(s => s == feed.SocialChannel.Id))
                {
                }
                else
                {

                    string msg = showedList.Count == 0 ? "پست ارسال نشد ، جهت ارسال به کانال زیر ابتدا باید این ربات را در کانال یا گروه خود عضو کرده و آن را ادمین نمایید" + "\n" : "";
                    //todo: must logged
                    await Bot.Api.SendTextMessageAsync(
                        chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                        text: msg +
                              feed.SocialChannel.ChatTitle);
                    showedList.Add(feed.SocialChannel.Id);
                }
            }

        }

        public async Task SavePosts()
        {
            foreach (var telegramPost in _Posts)
            {
                telegramPost.FeedId = _feed.Id;
                _context.TelegramPosts.Add(telegramPost);
            }

            await _context.SaveChangesAsync();
        }
    }
}