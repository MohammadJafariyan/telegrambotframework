﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    public class ChannelService : BaseService<SocialChannel>
    {
        public List<Feed> GetTelegramSourceIfExists(int userId, long chatId)
        {
            using (var db = new BotDbContext())
            {
                var channels = db.SocialChannels.Include(f => f.Feeds)
                    .Where(w =>
                        w.TelegramUserId == userId).AsNoTracking().AsQueryable();

                return channels.SelectMany(c => c.Feeds).Where(f => f.Type == FeedType.Telegram
                                                                    && f.TelegramChatId == chatId).ToList();
            }
        }

        public static string GetAtsignName(string channelAddress)
        {
            return channelAddress;
        }

        public static string GetUrlName(string channelAddress)
        {
            return channelAddress;
        }

      
    }


    public class BaseService<T> where T : class, new()
    {
        private DbSet<T> _entities { get; set; }

        public InlineKeyboardButton[][] ConvertToInlineKeybord(List<T> channels, string propertyName, string uniqName)
        {
            List<InlineKeyboardButton> buttons = new List<InlineKeyboardButton>();
            int i = 0;

            // foreach for converting objects to InlineKeyboardButton
            while (i < channels.Count)
            {
                dynamic m = channels[i];

                // which property for name ?
                if (propertyName == null || channels[i].GetType().GetProperty(propertyName) == null)
                {
                    throw new Exception("propertyName null");
                }

                // which property for name ?
                var value = channels[i].GetType().GetProperty(propertyName)?.GetValue(channels[i], null);
                var button =
                    InlineKeyboardButton.WithCallbackData(value?.ToString() ?? "خالی",
                        uniqName + "_" + m.Id.ToString());

                buttons.Add(button);
                i++;
            }

            var toDimentialArray = ConvertToTable<InlineKeyboardButton>(buttons, 2);
            return toDimentialArray;
        }

        private T1[][] ConvertToTable<T1>(List<T1> buttons, int columns)
        {
            Stack<T1> stack = new Stack<T1>();
            foreach (var button in buttons)
            {
                stack.Push(button);
            }

            var arr = new List<T1[]>();
            int i = 0;

            while (stack.Count > 0)
            {
                var tempArrr = new List<T1>();
                for (int j = 0; j < columns; j++)
                {
                    if (stack.Count > 0)
                    {
                        tempArrr.Add(stack.Pop());
                    }
                    else
                    {
                        break;
                    }
                }

                arr.Add(tempArrr.ToArray());
            }

            /*while (i < buttons.Count)
            {
                
                // اگر تعداد شمارشگر بیشتر از تعداد آرایه باشد
                if ( i+columns>buttons.Count-1)
                {
                   var t =i- columns;
                    // چه تعداد بیشتر است ؟
                    var diff=buttons.Count-1 - (t + columns);
                    //تعداد بیشتر را از تعداد ستون ها کم کن
                    columns= columns - diff;
                }
                
                // start from i
                // until reaching count of columns form i
                var tempArr = new EntityList<T1>();
                for (int j = i; j < i + columns; j++)
                {
                    tempArr.Add(buttons[j]);
                }

                arr.Add(tempArr.ToArray());
                i += columns;
            }*/

            return arr.ToArray();
        }


        public void DeleteById(int id)
        {
            using (var db = new BotDbContext())
            {
                _entities = db.Set<T>();
                var record = _entities.Find(id);

                if (record == null)
                {
                    throw new Exception("not found");
                }

                db.Entry(record).State = EntityState.Deleted;
                db.SaveChanges();
            }
        }

        public List<T> GetAll()
        {
            using (var db = new BotDbContext())
            {
                _entities = db.Set<T>();
                return _entities.AsNoTracking().ToList();
            }
        }

        public int Save(T t)
        {
            using (var db = new BotDbContext())
            {
                _entities = db.Set<T>();
                _entities.Add(t);
                db.SaveChanges();
                dynamic m = t;
                return m.Id;
            }
        }

        public void Update(T modifiedRecord)
        {
            dynamic m = modifiedRecord;
            using (var db = new BotDbContext())
            {
                _entities = db.Set<T>();
                var record = _entities.Find(m.Id);
                if (record == null)
                {
                    throw new RecordNotFoundException();
                }

                db.Entry(record).CurrentValues.SetValues(modifiedRecord);
                db.Entry(record).State = EntityState.Modified;

                db.SaveChanges();
            }
        }

        public T FindById(int id)
        {
            using (var db = new BotDbContext())
            {
                _entities = db.Set<T>();
                var record = _entities.Find(id);
                if (record == null)
                {
                    throw new RecordNotFoundException();
                }

                return record;
            }
        }
    }

    public class RecordNotFoundException : Exception
    {
    }
}