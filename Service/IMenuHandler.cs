﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Args;

namespace TelegramBotsWebApplication.Service
{
    
    /// <summary>
    ///  هر handler یا منویی که بخواهد با تلگرام کار کند این اینترفیس را پیاده می کند
    /// </summary>
    public interface IMenuHandler
    {
        void BotOnMessageReceived(object sender, MessageEventArgs e);
        void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs e);
        void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs e);
        void BotOnReceiveError(object sender, ReceiveErrorEventArgs e);
        void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs e);
        void ShowInitialMenu(string msg);
        string UniqueName { get; }
    }
}
