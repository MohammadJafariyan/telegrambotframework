﻿using System;

namespace TelegramBotsWebApplication.Service
{
    public class CurrentRequestService
    {
        public string Token { get; set; }

        public int GetCurrentUserId()
        {
            if (string.IsNullOrEmpty(Token))
            {
                throw new Exception("خطای دسترسی توکن خالی است ");
            }

            int userId;

            try
            {
                userId = MyGlobal.ValidateHash(Token);

                return userId;
            }
            catch (Exception e)
            {
                throw new Exception("خطای دسترسی");
            }
        }
    }
}