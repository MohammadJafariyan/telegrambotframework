﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    public class UserService
    {
        public static void SetNextStepToCome(int step, TemporaryData temporaryData = null)
        {
            using (var db = new BotDbContext())
            {
                var user = db.TelegramUsers
                    .Include(u => u.TemporaryDatas)
                    .First(u => u.Id == CurrentRequestHolderSingleton.CurrentRequest.User.Id);
                user.NextStepToCome = step;
                db.Entry(user).Property(u => u.NextStepToCome).IsModified = true;

                if (temporaryData != null)
                {
                    foreach (var userTemporaryData in user.TemporaryDatas.ToList())
                    {
                        db.Entry(userTemporaryData).State = EntityState.Deleted;
                    }

                    user.TemporaryDatas.Add(temporaryData);
                }

                db.SaveChanges();
            }
        }

        //09010894680
        /// <summary>
        /// ایجاد کاربر اگر موجود نباشد
        /// </summary>
        /// <param name="telegramUserId"></param>
        /// <param name="text"></param>
        /// <param name="username"></param>
        /// <param name="mobile"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        public dynamic CreateIfNotExist(int telegramUserId,
            string text = null,
            string username = null, string mobile = null,
            string firstName = null, string lastName = null)
        {
            string lastTyped = null;
            using (var db = new BotDbContext())
            {
                var user = db.TelegramUsers
                    .Include(u => u.TemporaryDatas).Where(u => u.TelegramUserId == telegramUserId).FirstOrDefault();
                if (user == null)
                {
                    user = new TelegramUser
                    {
                        TelegramUserId = telegramUserId,
                        Username = username,
                        PhoneNumber = mobile,
                        LastTypedText = text,
                        FirstName = firstName,
                        LastName = lastName
                    };
                    db.TelegramUsers.Add(user);

                    user.UserAccount = new UserAccount
                    {
                        IsActive = true,
                        UserAccountType = UserAccountType.Trivial,
                        ExpireDate = DateTime.Now.AddDays(7),
                        MaxFeedCount = 3,
                        MaxCheckInDayCount = 6
                    };

                    db.SaveChanges();
                }
                else
                {
                    /// این چک کردن بخاطر این است که این متد در چند جا بصورت مختلف استفاده شده است
                    if (!string.IsNullOrEmpty(text))
                    {
                        lastTyped = user.LastTypedText;
                        user.LastTypedText = text;
                        db.Entry(user).Property(u => u.LastTypedText).IsModified = true;
                        db.SaveChanges();
                    }
                }

                return new {user, lastTyped};
            }
        }


        public static void SetNextMenuNameToCome(int id, string o)
        {
            using (var db = new BotDbContext())
            {
                var user = db.TelegramUsers.Find(id);
                if (user == null)
                    throw new Exception("user isn ull");
                user.NextMenuNameToCome = o;
                db.SaveChanges();
            }
        }
    }
}