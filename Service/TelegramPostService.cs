﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TelegramBotsWebApplication.Areas.Admin.Service;
using TelegramBotsWebApplication.Models;

namespace TelegramBotsWebApplication.Service
{
    public class TelegramPostService : GenericService<TelegramPost>
    {
        public List<TelegramPost> GetRecentAddedNotSentPostsByTelegramUserId(int currUserId)
        {
            using (var db = new BotDbContext())
            {
                var oneMinBefore = DateTime.Now.AddMinutes(-5);
                var telegramPosts = db.TelegramPosts
                        .Where(t =>
                            t.DeliveredChannelIds == null &&
                            t.SendTime == null && t.IsSendNow.HasValue == false)
                        .Where(t => t.TelegramUserId == currUserId)
                    ;
                return telegramPosts.ToList();
            }
        }

        public List<TelegramPost> GetInStackPosts(int currUserId)
        {
            using (var db = new BotDbContext())
            {
                var telegramPosts = db.TelegramPosts
                        .Where(t =>
                            t.DeliveredChannelIds == null && t.IsSendNow.HasValue == false)
                        .Where(t => t.TelegramUserId == currUserId)
                    ;
                return telegramPosts.ToList();
            }
        }

        public async Task SetRecentForwardedPostsSendTime(int hour, int minute, int currUserId)
        {
            using (var db = new BotDbContext())
            {
                var telegramPosts = GetRecentAddedNotSentPostsByTelegramUserId(currUserId);

                var ids = telegramPosts.Select(t => t.Id).ToList();
                var postsforEdit = db.TelegramPosts.Where(t => ids.Contains(t.Id));

                foreach (var post in postsforEdit)
                {
                    post.SendTime = new DateTime(2019, 1, 1, hour, minute, 10);
                    db.Entry(post).Property(p => p.SendTime).IsModified = true;
                }

                await db.SaveChangesAsync();
            }
        }

        public async Task SetRecentForwardedPostsSendTime(bool isSendNow, int currUserId)
        {
            using (var db = new BotDbContext())
            {
                var telegramPosts = GetRecentAddedNotSentPostsByTelegramUserId(currUserId);

                var ids = telegramPosts.Select(t => t.Id).ToList();
                var postsforEdit = db.TelegramPosts.Where(t => ids.Contains(t.Id));

                foreach (var post in postsforEdit)
                {
                    post.IsSendNow = true;
                    db.Entry(post).Property(p => p.IsSendNow).IsModified = true;
                }

                await db.SaveChangesAsync();
            }
        }

        public List<TelegramPost> GetRecentAddedNotSentPostsByTelegramUserIdForSend(int currUserId)
        {
            using (var db = new BotDbContext())
            {
                var oneMinBefore = DateTime.Now.AddMinutes(-5);
                var telegramPosts = db.TelegramPosts
                    .Include(t => t.TelegramUser)
                    .Include("TelegramUser.TelegramChannels")
                    .Where(t => t.IsSendNow == true && t.DeliveredChannelIds == null)
                    .Where(t => t.TelegramUserId == currUserId).AsNoTracking().AsQueryable();
                return telegramPosts.ToList();
            }
        }

        public async void SetSentForForwardPosts(List<TelegramPost> forwardPosts, List<int> channels)
        {
            using (var db = new BotDbContext())
            {
                var ints = forwardPosts.Select(f => f.Id).ToList();
                var posts = await db.TelegramPosts
                    .Where(t => ints.Contains(t.Id)).ToListAsync();


                var socialChannels = db.SocialChannels.Where(s => channels.Contains(s.Id));

                string channelIds = MyGlobal.ExtractChannelIds(socialChannels);

                foreach (var telegramPost in posts)
                {
                    telegramPost.DeliveredChannelIds = channelIds;
                    db.Entry(telegramPost).Property(t => t.DeliveredChannelIds).IsModified = true;
                }

                await db.SaveChangesAsync();
            }
        }

        public async Task<List<TelegramPost>> GetAllUsersNotSendAsync(DateTime now, DateTime before)
        {
            using (var db = new BotDbContext())
            {
                var telegramPosts = await db.TelegramPosts
                    .Where(t => t.SendTime.HasValue &&
                                t.DeliveredChannelIds == null && t.IsSendNow.HasValue == false)
                    .ToListAsync();
                return telegramPosts.Where(t => t.SendTime.Value.Hour >= before.Hour &&
                                                t.SendTime.Value.TimeOfDay <= now.TimeOfDay).ToList();
            }
        }

        public List<TelegramPost> GetAllPostsByUserId(int currUserId)
        {
            using (var db = new BotDbContext())
            {
                var telegramPosts = db.TelegramPosts
                    .Where(t => t.TelegramUserId == currUserId)
                    .ToList();
                return telegramPosts;
            }
        }

        public async Task SaveAll(List<TelegramPost> posts)
        {
            using (var db = new BotDbContext())
            {
                foreach (var post in posts)
                {
                    post.TelegramUser=null;
                    db.TelegramPosts.Attach(post);
                    db.Entry(post).Property(p=>p._TelegramMessagePostIds).IsModified = true;


                }
                db.SaveChanges();
            }
        }
    }
}