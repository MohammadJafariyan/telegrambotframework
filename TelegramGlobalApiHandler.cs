﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBotsWebApplication.Models;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication.Handlers
{
    public class TelegramGlobalApiHandler
    {
        public static async Task SendPostToChannel(long socialChannelChatId, WebApiPost post)
        {
            // از نوع عکس است
            if (string.IsNullOrEmpty(post.ImageUrl) == false || post.ImageContent != null)
            {
                await Bot.Api.SendPhotoAsync(
                    socialChannelChatId,
                    new InputOnlineFile(post.ImageUrl),
                    post.Message
                );
            }
            else
            {
                await Bot.Api.SendTextMessageAsync(
                    chatId: socialChannelChatId,
                    text:
                    post.Message
                );
            }
        }

        public static async Task EditPostToChannel(long socialChannelChatId, WebApiPost post)
        {
            // از نوع عکس است
            if (string.IsNullOrEmpty(post.ImageUrl) == false || post.ImageContent != null)
            {
                if (!string.IsNullOrEmpty(post.Message))
                {
                    await Bot.Api.EditMessageCaptionAsync(
                        socialChannelChatId,
                        post.TelegramMessagePostId,
                        post.Message
                    );
                }

                if (post.ImageUrl != null)
                {
                    await Bot.Api.EditMessageMediaAsync(
                        12,
                        12,
                        new InputMediaPhoto(post.ImageUrl)
                    );
                }
            }
            else
            {
                await Bot.Api.EditMessageTextAsync(
                    socialChannelChatId,
                    post.TelegramMessagePostId,
                    post.Message
                );
            }
        }

        public static async Task DeletePostFromChannel(long socialChannelChatId, WebApiPost post)
        {
            await Bot.Api.DeleteMessageAsync(
                socialChannelChatId,
                post.TelegramMessagePostId
            );
        }
    }
}