﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Ajax.Utilities;
using Telegram.Bot.Types;
using TelegramBotsWebApplication.Handlers;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication.Models
{
    
    /// <summary>
    /// وقتی کاربر در تلگرام از بت یا به بت هر پیغامی رد و بدل میشود
    /// این کلاس پر میشود و از هر جای برنامه قابل دسترسی است
    /// اطلاعاتی مانند کاربر پیغام دهنده و آخرین نوشته و آخرین منو در آن ذخیره می شود
    /// </summary>
    public class CurrentRequestHolderSingleton
    {
        private static CurrentRequest _currentRequest;

        public static CurrentRequest CurrentRequest
        {
            get
            {
                if (_currentRequest == null)
                {
                    _currentRequest = new CurrentRequest();
                }

                return _currentRequest;
            }
        }


        /// <summary>
        /// اگر NextMenuNameToCome نال باشد در مرحله بعد منوی root  last typedاش نمایش داده می شود
        /// </summary>
        /// <param name="nextMenuNameToCome"></param>
        public static  void SaveStateAndShowNextToComeMenu(string nextMenuNameToCome)
        {
            using (var db = new BotDbContext())
            {
                var telegramUserId = CurrentRequestHolderSingleton.CurrentRequest.User.TelegramUserId;
                var record =  db.TelegramUsers
                    .Include(u => u.TemporaryDatas).Where(u => u.TelegramUserId == telegramUserId)
                    .First();

                record.LastTypedText = CurrentRequestHolderSingleton.CurrentRequest.TypedMessage;
                record.NextMenuNameToCome = nextMenuNameToCome;
                 db.SaveChanges();
                
                
                // بعد از اجرای عملیات ، منوی بعدی نمایش داده می شود
                ShowNextToComeMenu(nextMenuNameToCome,record.NextStepToCome);
            }

        }

        private static void ShowNextToComeMenu(string nextMenuNameToCome,int nextStepToCome)
        {
            var menu = MessageDispatcher.FindApproprateHandlerRecursive(nextMenuNameToCome,
                BigMenuTreeHolderSingleton.MenuTree);
      
            menu.Handler.ShowInitialMenu(null);
        }
    }

    public class CurrentRequest
    {
        public string TypedMessage { get; set; }
        public MenuTree LastMenuTree { get; set; }
        public MenuTree CurrentMenuTree { get; set; }
        public TelegramUser User { get; set; }
        public string LastTypedText { get; set; }

        /// <summary>
        /// این تابع مشخص می کند که در یک فرم ثبت کاربر در کدام مرحله است و به آن مرحله هدایت می شود
        /// یعنی انتخاب کننده CurrentMenuTree
        /// </summary>
        /// <returns></returns>
        public TemporaryData GetTemporary()
        {
            var t = User.TemporaryDatas.FirstOrDefault();
            if (t == null)
            {
                return new TemporaryData();
            }

            return t;
        }


        /// <summary>
        /// کاربر در مراجعات بعدی (یعنی پیغام های بعدی) به handler ای که
        /// در این متد داده می شود پاس داده خواهد شد
        /// </summary>
        /// <param name="handlerName"></param>
        public async Task SaveState(string handlerName)
        {
            using (var db=new BotDbContext())
            {
                var telegramUser = db.TelegramUsers.Find(User.Id);
                telegramUser.Handler = handlerName;
                User.Handler = handlerName;
                db.Entry(telegramUser).Property(u => u.Handler).IsModified = true;
                await db.SaveChangesAsync();
            }
        }


        public void Initialize(dynamic tuple, string text, Message eMessage)
        {
            User = tuple.user;
            LastTypedText = tuple.lastTyped;
            TypedMessage = text;

            CurrentMessage = eMessage;

            //اولین بار
            if (tuple.lastTyped == null)
            {
                // بر اساس text انتخاب
                CurrentMenuTree =
                    MessageDispatcher.FindApproprateHandlerRecursive(TypedMessage, BigMenuTreeHolderSingleton.MenuTree);
                return;
            }

            // آخرین منویی که کاربر در آن حضور داشت
            LastMenuTree =
                MessageDispatcher.FindApproprateHandlerRecursive(tuple.lastTyped, BigMenuTreeHolderSingleton.MenuTree);

// یعنی پیدا نکرده است
            if (LastMenuTree.Handler is HelloHandler)
            {
                CurrentMenuTree =
                    MessageDispatcher.FindApproprateHandlerRecursive(text, BigMenuTreeHolderSingleton.MenuTree);

                return;
            }
            if (LastMenuTree.IsForm)
            {
                CurrentMenuTree = LastMenuTree;
            }
            else
            {
                // آخرین ار فرم نبوده است

                // اگر آخرین بار ریدایرکت شده است
                if (!string.IsNullOrEmpty(tuple.user.NextMenuNameToCome) && tuple.user.NextMenuNameToCome!="/start")
                {
                    CurrentMenuTree = MessageDispatcher.FindApproprateHandlerRecursive(tuple.user.NextMenuNameToCome,
                        BigMenuTreeHolderSingleton.MenuTree);
                    UserService.SetNextMenuNameToCome(tuple.user.Id,null);
                }
                else
                {
                    CurrentMenuTree =
                        MessageDispatcher.FindApproprateHandlerRecursive(text, BigMenuTreeHolderSingleton.MenuTree);
                }
            }

            // 
        }

        public Message CurrentMessage { get; set; }

        public void InitializeForInlineQuery(string callbackQueryData)
        {

         string name=   MyGlobal.ExtractUniqueNameForHandler(callbackQueryData);
              CurrentMenuTree =
                                MessageDispatcher.FindApproprateHandlerRecursiveByInlineQuery
                                    (name, BigMenuTreeHolderSingleton.MenuTree);
                           
        }
    }
}
