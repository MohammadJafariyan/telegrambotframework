﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TelegramBotsWebApplication.Handlers;
using TelegramBotsWebApplication.Handlers.ChannelsSave;
using TelegramBotsWebApplication.Handlers.DefineSourceHandlers;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication.Models
{
    /// <summary>
    /// منو های تلگرام مورد استفاده قرار می گرفت
    /// اما با تغییر نیازمندی ها مورد استفاده قرار نمی گیرد
    /// </summary>
    public class BigMenuTreeHolderSingleton
    {
        public static readonly MenuTree MenuTree = new MenuTree
        {
            Name = "/start",
            Handler = new HelloHandler(),

            Leafs = new List<MenuTree>
            {
                new MenuTree
                {
                    Order = 1,
                    Name = "تعریف مقصد انتشار مطالب",
                    IsForm = false,
                    Leafs = new List<MenuTree>
                    {
                        new MenuTree
                        {
                            Order = 1,
                            Name = "ثبت اطلاعات کانال ها",
                            IsForm = true,
                            Handler = new DefineSourceTelegramHandler(),
                        },
                        new MenuTree
                        {
                            Order = 2,
                            Name = "نمایش کانالهای تلگرام",
                            IsForm = false,
                            Handler = new ChannelListHandler(),
                        },
                        new MenuTree
                        {
                            Order = 3,
                            Name = "حذف کانالهای تلگرام",
                            IsForm = false,
                            Handler = new ChannelDeleteHandler(),
                        }
                    }
                },
                new MenuTree
                {
                    Order = 2,
                    Name = "تعریف منابع اطلاعات",
                    IsForm = false,
                    Handler = new DefineSourceHomeHandler(),
                    Leafs = new List<MenuTree>
                    {
                        new MenuTree
                        {
                            Order = 1,
                            Name = "فرم ورود اطلاعات صفحه های اینستاگرام و تلگرام",
                            IsForm = false,
                            Handler = new DefineTelegramOrInstagramPagesHandler(),
                            Leafs = new List<MenuTree>
                            {
                                new MenuTree
                                {
                                    Order = 1,
                                    Name = "تعریف صفحه تلگرام",
                                    IsForm = true,
                                    Handler = new DefineInstagramPagesHandler(),
                                    Leafs = new List<MenuTree>
                                    {
                                    }
                                },
                                new MenuTree
                                {
                                    Order = 2,
                                    Name = "تعریف صفحه اینستاگرام",
                                    IsForm = true,
                                    Handler = new DefineTargetTelegramPagesHandler(),
                                    Leafs = new List<MenuTree>
                                    {
                                    }
                                }
                            }
                        },
                        new MenuTree
                        {
                            Order = 1,
                            Name = "فرم تایین رابطه بین هرمنبع با مقاصد مختلف",
                            IsForm = true,
                            Handler = new DefineRelationsHandler(),
                            Leafs = new List<MenuTree>
                            {
                            }
                        },
                    }
                },
                new MenuTree
                {
                    Order = 3,
                    Name = "فرایند خوانش و ارسال اطلاعات",
                    IsForm = false,
                    Leafs = new List<MenuTree>
                    {
                    }
                }
            }
        };

        public static IMenuHandler GetApproprateHandlerByName(string handler)
        {
            if (handler == "ForwardPostToChannelsHandler")
            {
                return new ForwardPostToChannelsHandler();
            }
            if (handler == "PostsInStackHandler")
            {
                return new PostsInStackHandler();
            }

            return null;
        }
    }
}