﻿using System.Collections.Generic;

namespace TelegramBotsWebApplication.Models
{

    /// <summary>
    /// تمامی خروجی وب سرویس ها
    /// </summary>
    /// <typeparam name="T">مدل جدول</typeparam>
    public class MyApiResponse<T>
    {
        public MyApiResponse()
        {
            Message = "==> Success = 1 & Fail=3";
        }
        /// <summary>
        /// اگر خروجی ما تنها یکی باشد
        /// </summary>
        public T Single { get; set; }

        /// <summary>
        /// اگر خروجی ما بصورت لیست باشد
        /// </summary>
        public List<T> List { get; set; }

        /// <summary>
        /// کل تعداد موجود در جدول
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// پیغام در صورت وجود خطا
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// نوع خروجی ، خطا یا موفق
        /// </summary>
        public MyApiResponseType Type { get; set; }
    }
}