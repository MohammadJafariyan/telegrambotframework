﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telegram.Bot;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication.Models
{
    /// <summary>
    /// هر منو بعنوان ساختار درختی بصورت زیر پیاده شده است
    /// </summary>
    public class MenuTree
    {
        public MenuTree()
        {
            _leafs=new List<MenuTree>();
        }

        private List<MenuTree> _leafs;
        
        public bool HasStepInside { get; set; }

        public int Order { get; set; }
        public bool IsForm { get; set; }

        public IMenuHandler Handler { get; set; }
        /// <summary>
        /// نام منو
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// نود پدر
        /// </summary>
        public MenuTree Root { get; set; }

        /// <summary>
        /// نود های بعدی
        /// </summary>
        public List<MenuTree> Leafs
        {
            get { return _leafs.OrderBy(l => l.Order).ToList(); }
            set
            {
                _leafs = value;
            }
        }

        public string DefaultBackMenuName { get; set; }
    }

    public static class Bot
    {

        public static readonly TelegramBotClient Api = new TelegramBotClient(
            
            // test bot
            System.Diagnostics.Debugger.IsAttached ? "1179787776:AAGdL7i9RDau7QJjAJ7tN5BPjr1x2sTmYbg"
            :

            "1114229478:AAGVikH-CG8BdSFDteXSZ6hqcPh735BhDpY");
    }
}