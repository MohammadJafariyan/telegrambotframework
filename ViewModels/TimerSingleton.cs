﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Timers;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication.Models
{
    public class TimerSingleton
    {
        private static Timer aTimer;

        public static void Start()
        {
            aTimer = new System.Timers.Timer();
            aTimer.Interval = 1 * 60 * 1000;
           // aTimer.Interval = 20 * 1000;

            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += TimerService.OnTimedEvent;

            // Have the timer fire repeated events (true is the default)
            aTimer.AutoReset = true;

            // Start the timer
            aTimer.Enabled = true;
        }

      }


   
}