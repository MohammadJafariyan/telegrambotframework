﻿namespace TelegramBotsWebApplication.Models
{
    public enum FeedType
    {
        WebRss,
        Hashtag,
        Instagram,
        Telegram,
        ForwardPostSetting
    }
    public enum WebApiPostStatus
    {
        
    }

    public enum MyApiResponseType
    {
        Success = 1,
        Fail = 3
    }

    public enum MyJsonResponseType
    {
        Success = 1,
        Fail = 2
    }
    public enum ChannelType
    {
        Source = 0,
        Target = 1
    }
    public enum UserAccountType
    {
        Trivial,Vip
    }

    public enum SocialChannelType
    {
        Telegram = 0,
        Instagram = 1
    }
}