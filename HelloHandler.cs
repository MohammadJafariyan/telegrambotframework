﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBotsWebApplication.Areas.Admin.Service;
using TelegramBotsWebApplication.DependencyInjection;
using TelegramBotsWebApplication.Models;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication.Handlers
{
    public class HelloHandler : IMenuHandler
    {

        public string UniqueName
        {
            get { return "HelloHandler"; }
        }

        public void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs e)
        {
        }

        public void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs e)
        {
        }

        public async void ShowInitialMenu(string msg = null)
        {
            var userAccountService = new UserAccountService();

            var userAccont= userAccountService.GetUserAccount(CurrentRequestHolderSingleton.CurrentRequest.User.Id);

            var suitableMessage = userAccountService.GetSuitableMessage(userAccont);

            InlineKeyboardMarkup ReplyKeyboard = null; /*new[]
            {
                /*new[]
                {
                    new InlineKeyboardButton
                    {
                        Text = "ویدئو معرفی",
                        Url = "ashpazerooz.ir",
                        CallbackData = "ویدئو معرفی"
                    },
                    new InlineKeyboardButton
                    {
                        Text = "راهنما",
                        CallbackData = "راهنما"
                    }
                },#1#
                /*new[]
                {
                    new InlineKeyboardButton
                    {
                        Text = "کانال های من",
                        CallbackData = "کانال های من"
                    },
                }#1#
            };*/
            /*ReplyKeyboardMarkup ReplyKeyboard = new[]
                          {
                              new[] {  "ثبت اطلاعات کانال ها"  ,"نمایش کانالهای تلگرام" },
                              new[] { "حذف کانالهای تلگرام"  },
                          };*/
            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text: " سلام به ربات تلگرامی آموزش مجازی بورس و فارکس دکتر فرشاد رضوان خوش آمدید" +
                      "\n" +
                      "\n" +
                      suitableMessage+
                      "\n" +
                      "\n" +
                      " ✅ "+ "اگر عضو هستید برای ورود به گروه آموزش خصوصی :" +
                      "\n" +
                      "\n" +
                      "/Join" +
                      "\n" +
                      "\n" +
                      " ✅ "+ "جهت عضویت یا پرداخت حق عضویت : " +
                      "\n" +
                      "\n" +
                      "/PayOrSignUp"+
                      
                      "\n" +
                      "\n" +

                      " ✅ "+  "ویدئو های آموزشی عمومی و اختصاصی بورس و فارکس :"+
                      "\n" +
                      "\n" +
                "/Videos"

                /*+
                                 "\n" +
                                 "\n" +
                                 " ✅ "+ "مشاهده لیست مطالب در صف انتظار برای ارسال:"+
                                 "\n" +
                                 "\n" +
                                 "/postsInStack"+
                                 "\n" +
                                 "\n" +
                                 " ✅ "+ "تنظیمات ارسال مطالب : "+
                                 "\n" +
                                 "\n" +
                                 "/postSendSetting"+
                                 "\n" +
                                 "\n" +
                                 " ✅ "+  "آمار ارسال و ویرایش و حذف در پنل : "+
                                 "\n" +
                                 "\n" +
                                 "/statAndChange"+
                                 "\n" */

                ,
                replyMarkup: ReplyKeyboard,parseMode:ParseMode.Default
            );
        }

        public void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs e)
        {
        }

        public async void BotOnMessageReceived(object sender, MessageEventArgs e)
        {
            var message = e.Message;
            if (message == null || message.Type != MessageType.Text) return;

            var currUser = CurrentRequestHolderSingleton.CurrentRequest.User;

            if (message.Chat.Type!=ChatType.Private)
            {
                try
                {

                    // کاربر جدید اضافه شده است
                    if (message.NewChatMembers?.Length>0)
                    {
                     
                        return;
                    }

                    
                    
                    
                    return;
                }
                catch (Exception exception)
                {
                    await Bot.Api.SendTextMessageAsync(
                        chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                        text: "ابتدا این ربات را به کانال خود اضافه نموده و تبدیل به ادمین کانال (یا گروه) نمایید"
                    );
                }
            }
            else
            {
                
                
                if (e.Message.Text == "/Join")
                {
                    //   await ShowStatAndChangeInPanel();


                 
                    await ShowJoinToMyChannel();
                    return;
                }


                if (System.Diagnostics.Debugger.IsAttached)
                {
                    if (e.Message.Text == "/token")
                    {
                        //   await ShowStatAndChangeInPanel();

                        
                        await Bot.Api.SendTextMessageAsync(
                            chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                            text:MyGlobal.Encrypt(currUser.Id.ToString())
                        );
                        await Bot.Api.SendTextMessageAsync(
                            chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                            text:"گالا"
                        );
                 
                        
                        return;
                    }
                }
                
                if (e.Message.Text == "/PayOrSignUp")
                {
                    await PayOrSignUpMyChannel();
                    /*new PostsInStackHandler().ShowInitialMenu(null);
                                      await CurrentRequestHolderSingleton.CurrentRequest
                                          .SaveState("PostsInStackHandler");*/
                    return;
                }
                
                if (e.Message.Text == "/Videos")
                {
                    await ShowMyVideos();
                    /*new PostsInStackHandler().ShowInitialMenu(null);
                                      await CurrentRequestHolderSingleton.CurrentRequest
                                          .SaveState("PostsInStackHandler");*/
                    return;
                }

            }

            ShowInitialMenu(null);


        }

        private async Task ShowMyVideos()
        {
            var settingService = new SettingService();

            var userAccountService = new UserAccountService();

            var userAccont= userAccountService.GetUserAccount(CurrentRequestHolderSingleton.CurrentRequest.User.Id);

            var suitableMessage = userAccountService.GetSuitableMessage(userAccont);


            
            string host =  "https://ashpazerooz.ir/";
            InlineKeyboardMarkup ReplyKeyboard = new[]
            {
                new[]
                {
                    new InlineKeyboardButton
                    {
                        Text = "نمایش ویدئو ها",
                        CallbackData = "ashpazerooz.ir",
                        Url =
                            $@"{settingService.GetMyChannel()}"
                    },
                }
            };
            
            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text:
                "جهت مشاهده ویدئو های آموزشی دکتر رضوان لمس نمایید ▼ ",
                replyMarkup: ReplyKeyboard
            );  
            
        }

        private async Task PayOrSignUpMyChannel()
        {
            var settingService = new SettingService();

            var userAccountService = new UserAccountService();

            var userAccont= userAccountService.GetUserAccount(CurrentRequestHolderSingleton.CurrentRequest.User.Id);

            var suitableMessage = userAccountService.GetSuitableMessage(userAccont);


            
            string host =  "https://ashpazerooz.ir/";
            InlineKeyboardMarkup ReplyKeyboard = new[]
            {
                new[]
                {
                    new InlineKeyboardButton
                    {
                        Text = userAccont.IsActive ? "ورود به کانال":"ثبت نام یا تمدید عضویت",
                        CallbackData = "ashpazerooz.ir",
                        Url =
                            $@"{settingService.GetMyChannel()}"
                    },
                }
            };
            
            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text: "برای ثبت نام یا تمدید عضویت دکمه زیر را لمس کنید ",
                replyMarkup: ReplyKeyboard
            );        }

        private async Task ShowJoinToMyChannel()
        {
            var settingService = new SettingService();

            var userAccountService = new UserAccountService();

            var userAccont= userAccountService.GetUserAccount(CurrentRequestHolderSingleton.CurrentRequest.User.Id);

            var suitableMessage = userAccountService.GetSuitableMessage(userAccont);


            
            string host =  "https://ashpazerooz.ir/";
            InlineKeyboardMarkup ReplyKeyboard = new[]
            {
                new[]
                {
                    new InlineKeyboardButton
                    {
                        Text = userAccont.IsActive ? "ورود به کانال":"ثبت نام یا تمدید عضویت",
                        CallbackData = "ashpazerooz.ir",
                        Url =
                            $@"{settingService.GetMyChannel()}"
                    },
                }
            };

            await Bot.Api.SendTextMessageAsync(
                CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                userAccont.IsActive ? "جهت ورود به کانال کمه زیر را لمس نمایید"  : suitableMessage + "\n" + "جهت عضویت دکمه زیر را لمس نمایید",
                replyMarkup: ReplyKeyboard
            );
        }

        private async Task ShowPostSetting(int currUserId)
        {

            string host =  "https://ashpazerooz.ir/";
            InlineKeyboardMarkup ReplyKeyboard = new[]
            {
                new[]
                {
                    new InlineKeyboardButton
                    {
                        Text = "تنظیمات ارسال مطالب",
                        CallbackData = "ashpazerooz.ir",
                        Url =
                            $@"{host}setting/ForwardPostSetting?hashed={MyGlobal.Encrypt(currUserId.ToString())}"
                    },
                }
            };

            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text: "جهت تعریف الگو ها و دیگر تنظیمات دکمه زیر را لمس نمایید",
                replyMarkup: ReplyKeyboard
            );
        }

        public static async Task ShowPostToChannelsStartMessage()
        {
            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text:
                " جهت ارسال مطلب به کانال ها ، لطفا پست های مورد نظر خود را به تعداد دلخواه به اینجا فوروارد نمایید  "
            );
        }

        private async Task ShowCurrentUserChannelsAsButton()
        {
            var currUser = CurrentRequestHolderSingleton.CurrentRequest.User;

            InlineKeyboardMarkup ReplyKeyboard = new[]
            {
                new[]
                {
                    new InlineKeyboardButton
                    {
                        Text = "پنل مدیریت",
                        CallbackData = "پنل مدیریت",
                        Url = $@"https://ashpazerooz.ir/setting/index?hashed={MyGlobal.Encrypt(currUser.Id.ToString())}"
                    },
                }
            };

            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text: " جهت ورود به پنل مدیریت دکمه زیر را لمس نمایید "
                ,
                replyMarkup: ReplyKeyboard
            );
        }
        
        
        private async Task ShowStatAndChangeInPanel()
        {
            var currUser = CurrentRequestHolderSingleton.CurrentRequest.User;

            InlineKeyboardMarkup ReplyKeyboard = new[]
            {
                new[]
                {
                    new InlineKeyboardButton
                    {
                        Text = "آمار ، حذف و ویرایش در پنل",
                        CallbackData = "پنل مدیریت",
                        Url = $@"https://ashpazerooz.ir/MyBotStatistics/MyChannels?hashed={MyGlobal.Encrypt(currUser.Id.ToString())}"
                    },
                }
            };

            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text: " جهت ورود به پنل مدیریت دکمه زیر را لمس نمایید "
                ,
                replyMarkup: ReplyKeyboard
            );
        }

        public void BotOnReceiveError(object sender, ReceiveErrorEventArgs e)
        {
        }

        public static async Task<Chat> IsChannelCorrectForBeingTargetForAdmin(string address, int telegramUserId)
        {
            string @chatId = address;

            if (address.Contains("t.me/"))
            {
                @chatId = MyGlobal.SplitAndGetRest(address, "t.me/");
            }

            var chatMembers = await Bot.Api.GetChatAdministratorsAsync(@chatId);
            if (chatMembers.All(c => c.User.Id != telegramUserId))
            {
                throw new Exception(
                    "ابتدا این ربات را به کانال خود اضافه نموده و تبدیل به ادمین کانال (یا گروه) نمایید");
            }


            return await Bot.Api.GetChatAsync(@chatId);
        }

        public static async Task<long> IsChannelCorrectForBeingSource(string address, int telegramUserId)
        {
            var @chatId = MyGlobal.GetTelegramChatId(address);

            var chat = await Bot.Api.GetChatAsync(@chatId);


            if (chat == null)
            {
                throw new Exception(
                    "ابتدا این ربات را به کانال خود اضافه نموده و تبدیل به ادمین کانال (یا گروه) نمایید");
            }

            return chat.Id;
        }
    }
}