﻿﻿using Microsoft.Owin;
using Owin;
using System;
using Telegram.Bot.Types.Enums;
using TelegramBotsWebApplication.Models;

[assembly: OwinStartupAttribute(typeof(TelegramBotsWebApplication.Startup))]
namespace TelegramBotsWebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

           
            
            var handler = new MessageDispatcher();
            Bot.Api.OnMessage += handler.BotOnMessageReceived;
            Bot.Api.OnMessageEdited += handler.BotOnMessageReceived;
            Bot.Api.OnCallbackQuery += handler.BotOnCallbackQueryReceived;
            Bot.Api.OnInlineQuery += handler.BotOnInlineQueryReceived;
            Bot.Api.OnInlineResultChosen += handler.BotOnChosenInlineResultReceived;
          //  Bot.Api.OnReceiveError += handler.BotOnReceiveError;


            Bot.Api.StartReceiving(Array.Empty<UpdateType>());
            
            
            TimerSingleton.Start();

        }
    }
}
