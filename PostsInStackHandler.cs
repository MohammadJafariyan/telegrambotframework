﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBotsWebApplication.Models;
using TelegramBotsWebApplication.Service;

namespace TelegramBotsWebApplication.Handlers
{
    public class PostsInStackHandler : IMenuHandler
    {
        private  ForwardPostCallerService _ForwardPostCallerService;
        private readonly TelegramPostService _telegramPostService = new TelegramPostService();

        public async void BotOnMessageReceived(object sender, MessageEventArgs e)
        {
            var message = e.Message;
            if (message == null || message.Type != MessageType.Text) return;

            var currUser = CurrentRequestHolderSingleton.CurrentRequest.User;

            if (message.ForwardDate != null)
            {
                return;
            }

            /*
            // جهت مشاهده
            string uniqIdForSee = $@"/UniqPost_number";

            // جهت ویرایش
            string uniqIdForEdit = $@"/UniqForEditPost_number";
            */
            if (e.Message.Text == "/listPosts")
            {
                ShowPostsAsList();
                return;

            }
            
            if (e.Message.Text.Contains("/UniqPost"))
            {
                var posts =  _telegramPostService.GetInStackPosts( currUser.Id);

                int id=await ParseIdForSee(e.Message.Text);
                if (id==-1)
                {
                    return;
                }


                var post = posts.FirstOrDefault(p => p.Id == id);
                if (post==null)
                {
                    await ShowNotFoundId();
                    return;
                }

                await ShowPost(post, currUser.Id);

                await ShowBackMenu();
                return;
            }
            if (e.Message.Text.Contains("/UniqForEditPost"))
            {
                await ShowNotWorkingMenu();
                await ShowBackMenu();

                return;
            }
            else
            {
                this.ShowInitialMenu(null);
                return;
            }

            await ShowBackMenu();


            return;
        }
        private async Task ShowPost(TelegramPost post, int currUserId)
        {
            try
            {

               await new ForwardPostCallerService().ShowForwardPosts(new List<TelegramPost>
               {
                   post
               },CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id, currUserId);
                
            }
            catch (Exception e)
            {
                try
                {
                    await Bot.Api.SendTextMessageAsync(
                        chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                        text: MyGlobal.RecursiveExecptionMsg(e)
                    );
                }
                catch (Exception exception)
                {

                    //todo:must be logged
                }
            }

        }

        private async Task<int> ParseIdForSee(string messageText)
        {
            try
            {
                int id = 0;
                string num=messageText.Split('_')[1];
                var isParsed = int.TryParse(num,out id);
                if (isParsed)
                {
                    return id;
                }
                await CantParse();
            }
            catch (Exception e)
            {
            }

            return -1;
        }

        private async Task CantParse()
        {
            InlineKeyboardMarkup ReplyKeyboard = new[]
            {
                new[]
                {
                    new InlineKeyboardButton
                    {
                        Text = "لغو و بازگشت",
                        CallbackData = "ویدئو معرفی"
                    }
                }
            };
            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text: "کد انتخاب شده صحیح نیست لطفا جهت ویرایش یا مشاهده پستی را انتخاب نمایید " +
                      "\n  " +
                      "در غیر اینصورت دکمه لغو و بازگشت را لمس نمایید",
                replyMarkup: ReplyKeyboard

            );
        }

        private async Task ShowBackMenu()
        {
            InlineKeyboardMarkup ReplyKeyboard = new[]
            {
                new[]
                {
                    new InlineKeyboardButton
                    {
                        Text = "لغو و بازگشت",
                        CallbackData = "ویدئو معرفی"
                    }
                }
            };
            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text: "در منوی صف انتظار برای ارسال قرار دارید جهت بازگشت دکمه زیر را لمس نمایید",
                replyMarkup: ReplyKeyboard

            );
        }


        private async Task ShowNotWorkingMenu()
        {
           
            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text: "فعلا این قابلیت در دست ساخت است"

            );
        }

        private async Task ShowNotFoundId()
        {
            await Bot.Api.SendTextMessageAsync(
                chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                text: "پست یافت نشد ، این پست یا ارسال شده است یا جزو پست های شما نیست  "
            );

        }


        public void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs e)
        {
        }

        public async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs e)
        {
                // به منزله بازگشت به منو اصلی
                await CurrentRequestHolderSingleton.CurrentRequest.SaveState(null);
                new HelloHandler().ShowInitialMenu();
                return;
        }

        public void BotOnReceiveError(object sender, ReceiveErrorEventArgs e)
        {
        }

        public void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs e)
        {
        }


        public async void ShowPostsAsList()
        {
            var currUser = CurrentRequestHolderSingleton.CurrentRequest.User;

            var posts = _telegramPostService.GetInStackPosts(currUser.Id);

            try
            {
                foreach (var telegramPost in posts)
                {
                    // فقط خلاصه ای نشان داده می شود
                    string text = telegramPost.Text?.Substring(0, telegramPost.Text.Length / 5) + "...";


                    // جهت مشاهده
                    string uniqIdForSee = $@"/UniqPost_{telegramPost.Id}";

                    // جهت ویرایش
                    string uniqIdForEdit = $@"/UniqForEditPost_{telegramPost.Id}";

                    string time = telegramPost.SendTime.HasValue
                        ? "زمان ارسال :"+ $@"  ساعت {telegramPost.SendTime.Value.Hour} و {telegramPost.SendTime.Value.Minute} دقیقه " + "\n" : "";
                    string t1 = "پست شماره :" + telegramPost.Id;
                    t1 += "\n";
                    t1 +=  time;
                    t1 += "⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃";
                    t1 += "\n";
                    await Bot.Api.SendTextMessageAsync(
                        chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                        text: t1+ text +
                              "\n" +
                              "⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃⁃"+
                              "\n" +
                              "جهت مشاهده : " +
                              "\n" +
                              uniqIdForSee
                              +
                              "\n" +
                              "جهت ویرایش : " +
                              "\n" +
                              uniqIdForEdit 

                    );
                    Thread.Sleep(1000);
                }
               
            }
            catch (Exception e)
            {
            }

            await ShowBackMenu();

        }
        public async void ShowInitialMenu(string msg)
        {
            var currUser = CurrentRequestHolderSingleton.CurrentRequest.User;

            var posts = _telegramPostService.GetInStackPosts(currUser.Id);

            try
            {
                var groupBy = posts.Where(p=> p.SendTime.HasValue).GroupBy(p => p.SendTime.Value.TimeOfDay);
                 msg = "";
                foreach (var map in groupBy)
                {

                    string time = $@"در ساعت {map.Key.Hours} و {map.Key.Minutes} دقیقه  ";
                    string count = map.Count()+"";
                    msg += time + count + " پست " + "\n";
                }

                msg += "\n";
                msg += "مشاهده بصورت لیست :";
                msg += "/listPosts";

                await Bot.Api.SendTextMessageAsync(
                    chatId: CurrentRequestHolderSingleton.CurrentRequest.CurrentMessage.Chat.Id,
                    text:msg
                );

            }
            catch (Exception e)
            {
            }

            await ShowBackMenu();

        }

        public string UniqueName
        {
            get { return nameof(PostsInStackHandler); }
        }
    }
}